<nav class="navbar navbar-transparent container-fluid internav">
                <button class="btn btn-info log-in-btn loner"><i class="glyphicon glyphicon-user"></i> LOG IN</button>
    
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    @if(Auth::user() && Auth::user()->qIsAdmin == 0)
                    <?php
                        $counter = DB::table('chatmessages')
                        ->where('qProfOIDReceiver', '=', Auth::user()->qProfOID)
                        ->where('qSeen', '=', 0)
                        ->groupBy('qChatOID')
                        ->get();

                        if(count($counter) > 0)
                        {
                            $returnhtml = '<i class="glyphicon glyphicon-envelope"></i> <span class="badge">'.count($counter).'</span>';
                        } else {
                            $returnhtml = '<i class="glyphicon glyphicon-envelope"></i>';
                        }
                    ?>
                    <a class="navbar-brand pull-right" id="chat-messages-envelope-responsive" href="{{ url('messages') }}">{!! $returnhtml !!}</a>
                    @endif
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav interbear">
                    <li><a><img src="images/bear.png"></a></li>
                </ul>


                    <ul class="nav navbar-nav navbar-right mainnav">
                            <li><a href="{{ url('/') }}">Start</a></li>
                            @if ($pageSlug == "")
                            <li><a href="#howhetwerkt">How het werkt</a></li>
                            <li><a href="#video">Video</a></li>
                            {{-- <li><a href="{{ url('help') }}">Help</a></li> --}}
                            <li><a href="{{ url('/register') }}">Inschrijven</a></li>
                            <li><a href="#contact">Contact</a></li>
                            @else
                            <li><a href="{{ url('/') }}/#howhetwerkt">How het werkt</a></li>
                            <li><a href="{{ url('/') }}/#video">Video</a></li>
                            {{-- <li><a href="{{ url('help') }}">Help</a></li> --}}
                            <li><a href="{{ url('/register') }}">Inschrijven</a></li>
                            <li><a href="{{ url('/') }}/#contact">Contact</a></li>
                            @endif
                            <li class="login"><a href="{{ url('login') }}">login</a></li>
                            <li class="login-li"><button class="btn btn-info log-in-btn"><i class="glyphicon glyphicon-user"></i> LOG IN</button></li>
                    </ul>
                </div>
            </div>
        </nav>