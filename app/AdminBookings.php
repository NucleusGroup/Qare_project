<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminBookings extends Model {

	public $timestamps = false;

	protected $table = 'activbookings';

	public function user()
	{
		return $this->belongsTo('App\User', 'qProfOIDBookingCreated', 'qActivOID');
	}

	public function activity()
	{
		return $this->belongsTo('App\AdminActivity', 'qActivOID', 'qActivOID');
	}

}
