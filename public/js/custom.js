$(function () {
        
    $(document).on('click', 'a[href^="#"]', function(e) {

        // $('.post-cat').each(function() {
        //     $(this).removeClass('active');  
        // });

        $('.cats.row.full').removeClass('active');

        var id = $(this).attr('href');

        var $id = $(id);
        if ($id.length === 0) {
            return;
        }

        e.preventDefault();

        var pos = $(id).offset().top;
        
        $('body, html').animate({scrollTop: pos + 5}, function() {
             $('.cats.row.full').addClass('active');
        });

        // $('.post-cat').addClass('active');


    });


    function setBear() {
        var minus = 15;
        if ( ! $('body').hasClass('dashboard') ) minus = 30;
        var dashboardLeft = $('.bearmask').offset().left - 15;
        $('ul.bear').css('left', dashboardLeft);
        $('ul.nav.navbar-nav.bear').show();
    }
    setBear();
    $(window).resize(setBear);


    // function setContainerHeight() {
    //     if ($('.container-login').length) {
    //         if ($('.container-login').height() < window.innerHeight) {
    //             $('.container-login').height(window.innerHeight);
    //         } else {
    //             $('.container-login').attr('style', '');
    //         }
    //     }
    // }
    // setContainerHeight();
    // $(window).resize(setContainerHeight);


    // $('.btn.btn-default.btn-green-envelope').on('click', function() {
    //     $('#chatrequest').modal('show');
    // }); 

    $('body').on('click', '.input-group.actdate span', function(e) {
        e.preventDefault();
        $('.input-group.actdate input').toggle();
    });


});