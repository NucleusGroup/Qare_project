-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2016 at 02:59 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qare`
--

-- --------------------------------------------------------

--
-- Table structure for table `activbookings`
--

CREATE TABLE `activbookings` (
  `qActivOID` int(11) NOT NULL,
  `qProfOIDBookingCreated` smallint(6) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qStatus` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `activbookings`
--

-- --------------------------------------------------------

--
-- Table structure for table `activcomments`
--

CREATE TABLE `activcomments` (
  `qActivOID` int(11) NOT NULL,
  `qActivCommentsNID` int(11) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qProfOIDActivCreated` int(11) NOT NULL,
  `qComment` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `activcomments`
--


-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `qActivOID` int(11) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qIsActive` tinyint(1) NOT NULL,
  `qEnvOID` int(11) NOT NULL,
  `qProfOIDCreated` int(11) NOT NULL,
  `qTitle` char(50) COLLATE utf8_bin NOT NULL,
  `qDescription` varchar(255) COLLATE utf8_bin NOT NULL,
  `qImage` mediumblob NOT NULL,
  `qLocation` text COLLATE utf8_bin NOT NULL,
  `qDate` date NOT NULL,
  `qTime` time NOT NULL,
  `qPublicYN` tinyint(1) NOT NULL,
  `qOutsideYN` tinyint(1) NOT NULL,
  `qNofSpaces` tinyint(1) NOT NULL,
  `qURLLocation` varchar(255) COLLATE utf8_bin NOT NULL,
  `qDuration` tinyint(23) NOT NULL,
  `qPrice` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `activities`
--


-- --------------------------------------------------------

--
-- Table structure for table `activtags`
--

CREATE TABLE `activtags` (
  `qActivOID` int(11) NOT NULL,
  `qTagOID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `activtags`
--


-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `qChatOID` int(11) NOT NULL,
  `qProfOIDRequested` int(11) NOT NULL,
  `qProfOID` int(11) NOT NULL,
  `qStatus` tinyint(1) NOT NULL,
  `qCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `chat`
--



-- --------------------------------------------------------

--
-- Table structure for table `chatmessages`
--

CREATE TABLE `chatmessages` (
  `qChatMessageOID` int(11) NOT NULL,
  `qChatOID` int(11) NOT NULL,
  `qProfOIDSender` int(11) NOT NULL,
  `qProfOIDReceiver` int(11) NOT NULL,
  `qMessage` text COLLATE utf8_bin NOT NULL,
  `qSeen` tinyint(1) NOT NULL,
  `qCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `chatmessages`
--

-- --------------------------------------------------------

--
-- Table structure for table `enviroment`
--

CREATE TABLE `enviroment` (
  `qEnvOID` int(11) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qIsActive` tinyint(1) NOT NULL,
  `qEnvTitle` char(30) COLLATE utf8_bin NOT NULL,
  `qEnvDesc` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `envtags`
--

CREATE TABLE `envtags` (
  `qTagOID` int(11) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qIsActive` tinyint(1) NOT NULL,
  `qEnvOID` int(11) NOT NULL,
  `qTagDesc` char(30) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `envtags`
--


-- --------------------------------------------------------

--
-- Table structure for table `phonecodes`
--

CREATE TABLE `phonecodes` (
  `qOID` int(11) NOT NULL,
  `qProfOID` int(11) NOT NULL,
  `qCode` int(11) NOT NULL,
  `qCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `phonecodes`
--


-- --------------------------------------------------------

--
-- Table structure for table `profcomments`
--

CREATE TABLE `profcomments` (
  `qProfOID` int(11) NOT NULL,
  `qProfCommentsNID` int(11) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qProfOIDCreated` int(11) NOT NULL,
  `qComment` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `profcomments`
--


-- --------------------------------------------------------

--
-- Table structure for table `proffavorites`
--

CREATE TABLE `proffavorites` (
  `qProfOID` int(11) NOT NULL,
  `qProfFavoritesNID` int(11) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qTypeOfFavorite` tinyint(1) NOT NULL,
  `qOIDFavorite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `proffavorites`
--

-----------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `qProfOID` int(11) NOT NULL,
  `qCreatedAt` datetime NOT NULL,
  `qModifiedAt` datetime NOT NULL,
  `qIsActive` tinyint(1) NOT NULL,
  `qEnvOID` int(11) NOT NULL,
  `qTypeOfProfile` smallint(6) NOT NULL,
  `qEmail` varchar(255) COLLATE utf8_bin NOT NULL,
  `qEmailHash` varchar(124) COLLATE utf8_bin NOT NULL,
  `qPassword` varchar(255) COLLATE utf8_bin NOT NULL,
  `qRememberToken` varchar(255) COLLATE utf8_bin NOT NULL,
  `qNameFirst` varchar(255) COLLATE utf8_bin NOT NULL,
  `qNamePrefix` varchar(255) COLLATE utf8_bin NOT NULL,
  `qNameLast` varchar(255) COLLATE utf8_bin NOT NULL,
  `qDateOfBirth` date NOT NULL,
  `qGender` tinyint(1) NOT NULL,
  `qAddress` varchar(255) COLLATE utf8_bin NOT NULL,
  `qZipCode` varchar(255) COLLATE utf8_bin NOT NULL,
  `qCity` varchar(255) COLLATE utf8_bin NOT NULL,
  `qPicture` mediumblob NOT NULL,
  `qPhoneNumber` varchar(255) COLLATE utf8_bin NOT NULL,
  `qURLFacebook` varchar(255) COLLATE utf8_bin NOT NULL,
  `qURLTwitter` varchar(255) COLLATE utf8_bin NOT NULL,
  `qURLLinkedIn` varchar(255) COLLATE utf8_bin NOT NULL,
  `qURLInstagram` varchar(255) COLLATE utf8_bin NOT NULL,
  `qURLOther` varchar(255) COLLATE utf8_bin NOT NULL,
  `qEmailFriend` varchar(255) COLLATE utf8_bin NOT NULL,
  `qEmailVerifiedYN` tinyint(1) NOT NULL,
  `qPhoneVerifiedYN` tinyint(1) NOT NULL,
  `qSocialMediaVerifiedYN` tinyint(1) NOT NULL,
  `qIDVerifiedYN` tinyint(1) NOT NULL,
  `qVOGVerifiedYN` tinyint(1) NOT NULL,
  `qProfileMemberLevel` tinyint(1) NOT NULL,
  `qProfileLevel` tinyint(1) NOT NULL,
  `qDescription` text COLLATE utf8_bin NOT NULL,
  `qInterest` text COLLATE utf8_bin NOT NULL,
  `qAnswers[5]` text COLLATE utf8_bin NOT NULL,
  `qPrefMaleYN` tinyint(1) NOT NULL,
  `qPrefFemaleYN` tinyint(1) NOT NULL,
  `qPrefInsideYN` tinyint(1) NOT NULL,
  `qPrefOutsideYN` tinyint(1) NOT NULL,
  `qPrefIndividualYN` tinyint(1) NOT NULL,
  `qPrefGroupYN` tinyint(1) NOT NULL,
  `qPrefDistance` int(11) NOT NULL,
  `qPrefProfileLevel` tinyint(1) NOT NULL,
  `qIsAdmin` tinyint(4) NOT NULL,
  `qIsDeleted` tinyint(1) NOT NULL,
  `qLocation` varchar(100) COLLATE utf8_bin NOT NULL,
  `qNotification` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`qProfOID`, `qCreatedAt`, `qModifiedAt`, `qIsActive`, `qEnvOID`, `qTypeOfProfile`, `qEmail`, `qEmailHash`, `qPassword`, `qRememberToken`, `qNameFirst`, `qNamePrefix`, `qNameLast`, `qDateOfBirth`, `qGender`, `qAddress`, `qZipCode`, `qCity`, `qPicture`, `qPhoneNumber`, `qURLFacebook`, `qURLTwitter`, `qURLLinkedIn`, `qURLInstagram`, `qURLOther`, `qEmailFriend`, `qEmailVerifiedYN`, `qPhoneVerifiedYN`, `qSocialMediaVerifiedYN`, `qIDVerifiedYN`, `qVOGVerifiedYN`, `qProfileMemberLevel`, `qProfileLevel`, `qDescription`, `qInterest`, `qAnswers[5]`, `qPrefMaleYN`, `qPrefFemaleYN`, `qPrefInsideYN`, `qPrefOutsideYN`, `qPrefIndividualYN`, `qPrefGroupYN`, `qPrefDistance`, `qPrefProfileLevel`, `qIsAdmin`, `qIsDeleted`, `qLocation`, `qNotification`) VALUES
(1, '2016-03-14 17:34:30', '2016-03-19 07:12:54', 0, 0, 0, 'qare-project@qare.com', '', '$2y$10$Jfw0/eqGZASzlshnpI9pYu7uIx9xpBV0MdOeWwE9f/sMnd3BKPX.6', '', 'Qare', '', 'Project', '1970-01-03', 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '', 0);
-- --------------------------------------------------------

--
-- Table structure for table `profileview`
--

CREATE TABLE `profileview` (
  `qViewOID` int(11) NOT NULL,
  `qProfOID` int(11) NOT NULL,
  `qProfOIDViewedBy` int(11) NOT NULL,
  `qCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `profileview`
--

-- --------------------------------------------------------

--
-- Table structure for table `proftags`
--

CREATE TABLE `proftags` (
  `qProfOID` int(11) NOT NULL,
  `qTagOID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `proftags`
--



-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `questionTitle` varchar(250) NOT NULL,
  `questionText` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedAt` datetime NOT NULL,
  `parentId` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--


-- --------------------------------------------------------

--
-- Table structure for table `searchactiv`
--

CREATE TABLE `searchactiv` (
  `qSearchOID` int(11) NOT NULL,
  `qLocation` varchar(200) COLLATE utf8_bin NOT NULL,
  `qDistance` int(11) NOT NULL,
  `qIsHome` tinyint(1) NOT NULL,
  `qIsPublic` tinyint(1) NOT NULL,
  `qIsInside` tinyint(1) NOT NULL,
  `qIsOutside` tinyint(1) NOT NULL,
  `qTags` text COLLATE utf8_bin NOT NULL,
  `qDays` varchar(200) COLLATE utf8_bin NOT NULL,
  `qDate` date NOT NULL,
  `qCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `qProfOID` int(11) NOT NULL,
  `qCounter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `searchactiv`
--


-- --------------------------------------------------------

--
-- Table structure for table `searchprof`
--

CREATE TABLE `searchprof` (
  `qSearchOID` int(11) NOT NULL,
  `qLocation` varchar(200) COLLATE utf8_bin NOT NULL,
  `qDistance` int(11) NOT NULL,
  `qTags` text COLLATE utf8_bin NOT NULL,
  `qIsMale` tinyint(4) NOT NULL,
  `qIsFemale` tinyint(4) NOT NULL,
  `qCreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `qProfOID` int(11) NOT NULL,
  `qCounter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `searchprof`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `activbookings`
--
ALTER TABLE `activbookings`
  ADD PRIMARY KEY (`qActivOID`,`qProfOIDBookingCreated`);

--
-- Indexes for table `activcomments`
--
ALTER TABLE `activcomments`
  ADD PRIMARY KEY (`qActivOID`,`qActivCommentsNID`);

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`qActivOID`),
  ADD UNIQUE KEY `qEnvOID` (`qEnvOID`,`qActivOID`);

--
-- Indexes for table `activtags`
--
ALTER TABLE `activtags`
  ADD PRIMARY KEY (`qActivOID`,`qTagOID`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`qChatOID`);

--
-- Indexes for table `chatmessages`
--
ALTER TABLE `chatmessages`
  ADD PRIMARY KEY (`qChatMessageOID`);

--
-- Indexes for table `enviroment`
--
ALTER TABLE `enviroment`
  ADD PRIMARY KEY (`qEnvOID`);

--
-- Indexes for table `envtags`
--
ALTER TABLE `envtags`
  ADD PRIMARY KEY (`qTagOID`),
  ADD UNIQUE KEY `qEnvOID` (`qEnvOID`,`qTagOID`);

--
-- Indexes for table `phonecodes`
--
ALTER TABLE `phonecodes`
  ADD PRIMARY KEY (`qOID`);

--
-- Indexes for table `profcomments`
--
ALTER TABLE `profcomments`
  ADD PRIMARY KEY (`qProfOID`,`qProfCommentsNID`);

--
-- Indexes for table `proffavorites`
--
ALTER TABLE `proffavorites`
  ADD PRIMARY KEY (`qProfOID`,`qProfFavoritesNID`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`qProfOID`),
  ADD UNIQUE KEY `qProfOID` (`qProfOID`,`qEnvOID`);

--
-- Indexes for table `profileview`
--
ALTER TABLE `profileview`
  ADD PRIMARY KEY (`qViewOID`);

--
-- Indexes for table `proftags`
--
ALTER TABLE `proftags`
  ADD PRIMARY KEY (`qProfOID`,`qTagOID`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `searchactiv`
--
ALTER TABLE `searchactiv`
  ADD PRIMARY KEY (`qSearchOID`);

--
-- Indexes for table `searchprof`
--
ALTER TABLE `searchprof`
  ADD PRIMARY KEY (`qSearchOID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `qActivOID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `qChatOID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `chatmessages`
--
ALTER TABLE `chatmessages`
  MODIFY `qChatMessageOID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `enviroment`
--
ALTER TABLE `enviroment`
  MODIFY `qEnvOID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `envtags`
--
ALTER TABLE `envtags`
  MODIFY `qTagOID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `phonecodes`
--
ALTER TABLE `phonecodes`
  MODIFY `qOID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `qProfOID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `profileview`
--
ALTER TABLE `profileview`
  MODIFY `qViewOID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=348;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `searchactiv`
--
ALTER TABLE `searchactiv`
  MODIFY `qSearchOID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `searchprof`
--
ALTER TABLE `searchprof`
  MODIFY `qSearchOID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
