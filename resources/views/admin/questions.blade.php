@extends('app')

@section('content')

<div class="container">

	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			@if (session('flash_message'))
			<div class="alert alert-success">{{ session('flash_message') }}</div>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<a class="btn btn-primary pull-right" href="{{ url('admin/question-add')}}">
				<i class="fa fa-trash"></i> Add question
			</a>
		</div>
	</div>

	<hr>

	<div class="row">
		<form method="get">
			<div class="col-xs-3 col-xs-offset-1">
				<label for="question">Question</label>
				<input type="text" name="question" class="form-control" value="{{ Request::get('question') }}">
			</div>
			<div class="col-xs-3">
				<label for="user">User</label>
				<input type="text" name="user" class="form-control" value="{{ Request::get('user') }}">
			</div>
			<div class="col-xs-3 col-xs-offset-1">
				<label for="user">&nbsp;</label><br>
				<input type="submit" class="btn btn-primary" value="Search">
			</div>
		</form>
	</div>

	<hr>

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Questions list</div>

				<div class="panel-body">
					<table class="table table-striped">

						<thead>
							<th>Title</th>
							<th>Created by</th>
							<th>Question date</th>
							<th>Details</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
						</thead>

						<tbody>
							@foreach ($questions as $question)
							<tr>
								<td>
									<div>{{ $question->questionTitle }} </div>
								</td>
								<td>
									<div>{{ App\User::find($question->user_id)->qNameFirst. ' ' .App\User::find($question->user_id)->qNameLast }}</div>
								</td>
								<td>
									<div>
										{{ date('d.m.Y. H:i', strtotime($question->createdAt)) }}
									</div>
								</td>
								<td>

									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#question-{{ $question->id }}">
										View
									</button>

									<div class="modal fade" id="question-{{ $question->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel"> {{ $question->questionTitle }}</h4>
												</div>
												<div class="modal-body">
													<div class="row">
														<div class="col-md-12">
															{{ $question->questionText }}
														</div>
													</div>
													<div class="row">
														<div class="col-md-8 col-md-offset-2">
															<h3>Write your answer</h3>
															<form class="form-horizontal" role="form" method="post" action="{{ url('admin/questions/'.$question->id) }}">
																<input type="hidden" name="_token" value="{{ csrf_token() }}">

																<input type="hidden" name="parentid" value="{{ $question->id }}">

																<div class="form-group">
																	<?php
																	$child = App\AdminQuestions::where('parentId', '=', $question->id)->first();
																	?>
																	<textarea class="form-control" name="question" rows="5">@if($child){{ $child->questionText }}@endif</textarea>
																</div>
																<div class="form-group">
																	<input type="submit" class="btn btn-primary" value="Save and send email">
																</div>

															</form>
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>

								</td>


								<td>
									<a href="{{ url('admin/question-edit/' . $question->id) }}" class="btn btn-info">
										<i class="fa fa-pencil"></i> Update
									</a>
								</td>

								<td>
									<a href="{{ url('admin/question-delete/' . $question->id) }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete question?')">
										<i class="fa fa-trash"></i> Delete
									</a>

								</td>
							</tr>
							@endforeach
						</tbody>
					</table>

					{!! str_replace('/?', '?', $questions->appends(['question' => Request::get('question'), 'user' => Request::get('user')])->render()) !!}
				</div>

			</div>
		</div>
	</div>
</div>
@endsection
