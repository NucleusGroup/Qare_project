@extends('app')

@section('content')

<div class="container">

	<div class="row">
		<div class="col-md-6 col-md-offset-2 text-center">
			<h3>Edit user</h3>
		</div>
	</div>

	@if(count($errors))
	<div class="row">
		<div class="col-md-6 col-md-offset-2 text-center">
			<div class="alert alert-danger">
				@foreach($errors->all() as $error)
				<p>{{ ($error) }}</p>
				@endforeach
			</div>
		</div>
	</div>
	@endif

	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<form method="post" enctype="multipart/form-data" action="{{ url('admin/users-edit/' . $user->qProfOID ) }}">


				<input type="hidden" name="_token" value="{{ csrf_token() }}">


				<div class="row">
					<div class="col-md-4">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="admin" value="1" @if($user->qIsAdmin == 1) {{ 'checked="ckecked"' }} @endif >
								Admin
							</label>
						</div>
					</div>
					<div class="col-md-4">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="active" value="1" @if($user->qIsActive == 0) {{ 'checked="ckecked"' }} @endif >
								Active
							</label>
						</div>
					</div>
				</div>

				<br>


				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="name">First Name</label>
							<input type="text" class="form-control" name="name" value="{{ $user->qNameFirst }}">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label for="lastname">Last Name</label>
							<input type="text" class="form-control" name="lastname" value="{{ $user->qNameLast }}">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="nameprefix">Name prefix</label>
							<input type="text" class="form-control" name="nameprefix" value="{{ $user->qNamePrefix }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="dateofbirth">Date of birth</label>
							<input type="text" class="form-control date" name="dateofbirth" value="{{ date('d.m.Y.', strtotime($user->qDateOfBirth)) }}">
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" class="form-control" name="email" value="{{ $user->qEmail }}">
						</div>
					</div>
				</div>


				<div class="form-group">
					<label for="dateofbirth">Gender &nbsp; &nbsp; &nbsp;</label>
					<div class="radio-inline">
						<label><input type="radio" value="0" name="gender" @if($user->qGender == 0) {{ 'checked="ckecked"' }} @endif>Not applicable</label>
					</div>
					<div class="radio-inline">
						<label><input type="radio" value="1" name="gender" @if($user->qGender == 1) {{ 'checked="ckecked"' }} @endif>Male</label>
					</div>
					<div class="radio-inline">
						<label><input type="radio" value="2" name="gender" @if($user->qGender == 2) {{ 'checked="ckecked"' }} @endif>Female</label>
					</div>
				</div>


				<div class="form-group">
					<div class="row">
						<div class="col-md-8 col-xs-8">
							<label>Location</label>
							<?php
								if($user->qCity !='' && $user->qZipCode != '')
								{
									$location = $user->qZipCode.', '.$user->qCity;
								} else {
									$location = '';
								}
							?>
							<input type="text" id="autocomplete" name="location" value="{{ $location }}" onFocus="geolocate()" class="form-control" placeholder="Search for city or zip code">
							<input type="hidden" name="latlon" value="{{ $user->qLocation }}" id="longitude">
						</div>
						<div class="col-md-4 col-xs-4">
							<label>Postal code</label>
							<input type="text" class="form-control" name="zipcode" id="postal_code" placeholder="Zip code" value="{{ $user->qZipCode }}">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="address">Address</label>
							<input type="text" class="form-control" name="address" value="{{ $user->qAddress }}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="address">City</label>
							<input type="text" class="form-control" name="city" value="{{ $user->qCity }}" id="city">
						</div>
					</div>
				</div>



				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="image">Profile picture</label>
							<input id="input-1" name="image" type="file" class="file-loading">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="profilememberlevel">Level of the member</label>
							<select name="profilememberlevel" class="form-control">
								<option value="0" @if($user->qProfileMemberLevel == 0) {{ 'selected="selected"' }} @endif>Light member</option>
								<option value="1" @if($user->qProfileMemberLevel == 1) {{ 'selected="selected"' }} @endif>Full member</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="phone">Phone number</label>
							<input type="text" class="form-control" name="phone" value="{{ $user->qPhoneNumber }}">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="facebook">Facebook</label>
							<input type="text" class="form-control" name="facebook" value="{{ $user->qURLFacebook }}">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="twitter">Twitter</label>
							<input type="text" class="form-control" name="twitter" value="{{ $user->qURLTwitter }}">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="linkedin">LinkedIn</label>
							<input type="text" class="form-control" name="linkedin" value="{{ $user->qURLLinkedIn }}">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="instagram">Instagram</label>
							<input type="text" class="form-control" name="instagram" value="{{ $user->qURLInstagram }}">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="other">Other</label>
							<input type="text" class="form-control" name="other" value="{{ $user->qURLOther }}">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="emailfriend">Email address of friend</label>
							<input type="email" class="form-control" name="emailfriend" value="{{ $user->qEmailFriend }}">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="description">Profile description</label>
							<textarea class="form-control" name="description" rows="5">{{ $user->qDescription }}</textarea>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="description">Interest</label>
							<textarea class="form-control" name="interest" rows="5">{{ $user->qInterest }}</textarea>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col-md-12">
						<label>Default preferences for search</label>
					</div>

					<div class="col-md-3">
						<div class="checkbox">
							<label for="malesearch">
								<input type="checkbox"  name="malesearch" value="1" @if($user->qPrefMaleYN == 1) {{ 'checked="ckecked"' }} @endif >
							Male</label>
						</div>
					</div>
					<div class="col-md-3">
						<div class="checkbox">
							<label for="individualsearch">
								<input type="checkbox"  name="individualsearch" value="1" @if($user->qPrefIndividualYN == 1) {{ 'checked="ckecked"' }} @endif >
							Individual</label>
						</div>
					</div>

					<div class="col-md-3">
						<div class="checkbox">
							<label for="insidesearch">
								<input type="checkbox"  name="insidesearch" value="1" @if($user->qPrefInsideYN == 1) {{ 'checked="ckecked"' }} @endif >
							Inside</label>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3">
						<div class="checkbox">
							<label for="outsidesearch">
								<input type="checkbox"  name="outsidesearch" value="1" @if($user->qPrefOutsideYN == 1) {{ 'checked="ckecked"' }} @endif >
								Outside</label>
						</div>
					</div>

					<div class="col-md-3">
						<div class="checkbox">
							<label for="femalesearch">
								<input type="checkbox"  name="femalesearch" value="1" @if($user->qPrefFemaleYN == 1) {{ 'checked="ckecked"' }} @endif >
							Female</label>
						</div>
					</div>


					<div class="col-md-3">
						<div class="checkbox">
							<label for="groupsearch">
								<input type="checkbox"  name="groupsearch" value="1" @if($user->qPrefGroupYN == 1) {{ 'checked="ckecked"' }} @endif >
							Group</label>
						</div>
					</div>
				</div>

				<br>

				<div class="row">
					<div class="col-md-5">
						<label for="defaultdistance">Default distance for search (km)</label>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<input type="number" class="form-control" name="defaultdistance" min="0" value="{{ $user->qPrefDistance }}">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" class="form-control" name="password">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="confirmpassword">Confirm password</label>
							<input type="password" class="form-control" name="confirmpassword">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<input type="submit" class="btn btn-block btn-primary" value="Save">
					</div>
				</div>

				<br>

			</form>
		</div>
	</div>
</div>

@endsection

@section('tailscripts')
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMAkuIuxWRb2fZ36YTL8a3RKO4tJ6TpGE&libraries=geometry,places&callback=initAutocomplete&language=en" async defer></script>
	<script>

		var placeSearch, autocomplete;

		function initAutocomplete() {

			autocomplete = new google.maps.places.Autocomplete(
				(document.getElementById('autocomplete')),
				{types: ['geocode']});

			autocomplete.addListener('place_changed', fillInAddress);
		}

		function fillInAddress() {
			var place = autocomplete.getPlace();
			$("#longitude").val(place.geometry.location.lat() + ', ' + place.geometry.location.lng());


			for (var i = 0; i < place.address_components.length; i++) {

				var addressType = place.address_components[i].types[0];

				if(addressType == 'locality')
				{
					$("#city").val(place.address_components[i].long_name);
				}

				if(addressType == 'postal_code')
				{
					$("#postal_code").val(place.address_components[i].long_name);
				} else {
					$("#postal_code").val('');
				}

			}
		}

		function geolocate() {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var geolocation = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};
					var circle = new google.maps.Circle({
						center: geolocation,
						radius: position.coords.accuracy
					});
					autocomplete.setBounds(circle.getBounds());
				});
			}
		}
	</script>

<script type="text/javascript">
	$(document).on('ready', function() {
		$("#input-1").fileinput(
			@if($user->qPicture)
			{
				initialPreview: [
				'<img src="{{ url('images/users/'.$user->qPicture) }}" class="file-preview-image">'
				],
				initialCaption: "{{ $user->qPicture }}"
			}
			@endif
			);
	});
</script>
@endsection
