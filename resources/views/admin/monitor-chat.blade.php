@extends('app')
<style type="text/css">
.row .message
{
  margin-top: 10px;
  margin-left: 30px;
  margin-right: 30px;
  border-bottom: 1px solid #0099ff;
}

.row .message img {
  max-height: 90;
  max-width: 90;
  height: auto;
  width: 90px;
  float: left;
  margin-right: 10px;
  -moz-border-radius: 3px;
  border-radius: 3px;

  }
</style>
@section('content')

<div class="container">

  <div class="row">
    <div class="col-xs-10 col-xs-offset-1">
      @if (session('flash_message'))
      <div class="alert alert-success">{{ session('flash_message') }}</div>
      @endif
    </div>
  </div>

  <hr>

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Monitor chat</div>

        <div class="panel-body">

        <?php
          if(count($messages) > 0)
          {
              $returnhtml = '';

                  $chat = DB::table('chatmessages')->where('qChatOID', '=', $id)->get();

                  $checkforlast = DB::table('chatmessages')->where('qChatOID', '=', $id)->orderBy('qCreatedAt', '=', $id)->first();

                foreach ($chat as $message) {

                  if($message->qProfOIDSender == $uid)
                  {
                    $idc = $message->qChatOID;
                    $idu = $message->qProfOIDSender;
                    $idus = $message->qProfOIDReceiver;
                  } else {
                    $idc = $message->qChatOID;
                    $idus = $message->qProfOIDSender;
                    $idu = $message->qProfOIDReceiver;
                  }

                  $sender = App\User::find($message->qProfOIDSender);

                  if($sender->qPicture)
                  {
                      $userpicture = '<img src="'.url('images/users/thumbs/'.$sender->qPicture).'" width="80">';
                  } else {
                      if($sender->qGender==0 || $sender->qGender==1)
                      {
                        $userpicture = '<img src="'.url('img/user-male.png').'" width="80">';
                      }
                      else
                      {
                        $userpicture = '<img src="'.url('img/user-female.png').'" width="80">';
                      }

                  }

                  if($sender->qProfOID == $uid)
                  {
                      $align = 'left';
                  } else {
                      $align = 'right';
                  }



                  if($align == 'left'){
                    $returnhtml .= '<div class="row message" style="text-align: '.$align.';">
                                        <div class="col-md-3">
                                          '.$userpicture.'
                                          <p>'.$sender->qNameFirst.' '.$sender->qNameLast.'</p>
                                        </div>
                                        <div class="col-md-9">
                                          <p>'.date('d.m.Y. H:i:s', strtotime($message->qCreatedAt)).'</p>
                                          <p>'.$message->qMessage.'</p>
                                        </div>
                                    </div>';
                  } else {
                    $returnhtml .= '<div class="row message" style="text-align: '.$align.';">
                                      <div class="col-md-9">
                                          <p>'.date('d.m.Y. H:i:s', strtotime($message->qCreatedAt)).'</p>
                                          <p>'.$message->qMessage.'</p>
                                      </div>
                                      <div class="col-md-3">
                                          '.$userpicture.'
                                          <p>'.$sender->qNameFirst.' '.$sender->qNameLast.'</p>
                                      </div>
                                  </div>';
                  }
              }
          } else {
              $returnhtml = "<h3>No messages.</h3>";
          }
          ?>
          <form method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div id="last_id"> <input type="hidden" name="last_id" value="@if($checkforlast) {{ $checkforlast->qChatMessageOID }} @else {{ 0 }} @endif"></div>
          </form>
          <div class="row">
            <div class="col-md-12" id="chat-window">
              <?php echo $returnhtml; ?>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
</div>
@endsection

@section('tailscripts')
<script type="text/javascript">

  $(document).ready(function() {
    pullData();
  });

  function pullData()
  {
    retrieveChatMessages();
    setTimeout(pullData,3000);
  }

  function retrieveChatMessages()
  {
    $.ajax({
      url: '{{ url('admin/messages/'.$idc.'/'.$idu.'/'.$idus.'/retrieveChatMessages') }}',
      type: 'post',
      headers:
      {
        'X-CSRF-Token': $('input[name="_token"]').val()
      },
      data: {
        'last_id'   : $('input[name="last_id"]').val()
      },
      success: function(response) {
        if(response.length > 0){
          var divs = response.split('+++');
          $('input[name="last_id"]').remove();
          $('#last_id').html(divs[0]);
          $("#chat-window").append(divs[1]);
        }
      }
    });
  }
</script>
@endsection
