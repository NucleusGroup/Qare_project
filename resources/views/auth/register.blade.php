@extends('app-front')

@section('content')
<div class="container-fluid container-login">
	<div class="row">

		@include('common.innernav')

		<div class="page-title text-center">
            <h1 class="register">Inschrijven</h1>
        </div>

		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		@if (session('flash_message'))
		<div class="alert alert-success">{{ session('flash_message') }}</div>
		@endif

		@if (session('error_message'))
		<div class="alert alert-danger">{{ session('error_message') }}</div>
		@endif

		<div class="login-form andregister text-center">
			<form class="form-horizontal" role="form" method="POST" action="{{ url('registerme') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-group">
					<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="EMAIL ADDRESS">
				</div>

				<div class="form-group">
					<input type="password" class="form-control" name="password" placeholder="WATCHWOORD">
				</div>

				<div class="form-group">
					<input type="password" class="form-control" name="password_confirmation" placeholder="CONFIRM WATCHWOORD">
				</div>

				<div class="form-group customcb">
						<input type="checkbox" id="remember" name="remember" value="1" checked="checked">
						<label for="remember"><span></span> Remember me&nbsp;&nbsp;</label>
				</div>

				<div class="form-group customcb">
						<input type="checkbox" id="location" name="location" value="1" checked="checked">
						<label for="location"><span></span> Use my location</label>
				</div>

				<button type="submit" class="btn btn-info log-in-submit icon">
					AANMELDEN
				</button>



			</form>

				<a href="{{ url('facebook') }}" class="btn btn-info btn-facebook icon register"><i class="fa fa-facebook" aria-hidden="true"></i>LOG IN MET FACEBOOK</a>

				<p class="login-link haveanaccount">Al een account? <a href="{{ url('/login') }}">Log in</a></p>
				
		</div>

	</div>
</div>
@endsection
