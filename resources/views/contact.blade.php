@extends('app-front')
<style>
html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 50%;
      }
</style>
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
		<div class="row">
			<div class="col-md-12 text-center">
				<h1 class="page-title">Contact us</h1>
			</div>

			<div class="col-md-12">
				<div id="map"></div>
				<br>
			</div>

			<div class="col-md-12 text-justify">
				<p>
					Eum ferri persequeris referrentur id, ut sea percipitur reprehendunt. Ancillae nominati mel ut, no accumsan appetere euripidis his. Ut sea vide omittam accusamus, no sit minimum definitiones. Scripta bonorum te eam.
				</p>
			</div>

			<div class="col-md-12">


					@if(count($errors))
					<div class="alert alert-danger">
						@foreach($errors->all() as $error)
							<p>{{ ($error) }}</p>
						@endforeach
					</div>
					@endif

					@if (session('flash_message'))
						<div class="alert alert-success">{{ session('flash_message') }}</div>
					@endif

					@if (session('error_message'))
						<div class="alert alert-danger">{{ session('error_message') }}</div>
					@endif

				<form method="post" url="{{ url('contact') }}">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="name">Name</label>
								<input type="text" id="name" class="form-control" placeholder="Name" name="name" value="">
							</div>
							<div class="form-group">
								<label for="email">Email</label>
								<input type="email" id="email" class="form-control" placeholder="Email" name="email" value="">
							</div>
							<div class="form-group">
								<label for="title">Title</label>
								<input type="text" id="title" class="form-control" placeholder="Title" name="title" value="">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="message">Message</label>
								<textarea id="message" class="form-control" rows="8" placeholder="Your message" name="message"></textarea>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6 col-md-offset-2 text-center dashboard">
							<button type="submit" name="contact_form" class="btn btn-block btn-primary">Send</button>
						</div>
					</div>

				</form>
			</div>

		</div>
	</div>
</div>

@endsection

@section('tailscripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMAkuIuxWRb2fZ36YTL8a3RKO4tJ6TpGE&callback=initMap"></script>
<script type="text/javascript">
	function initMap() {
		var myLatLng = {lat: 52.3702157, lng: 4.895167899999933};

		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 8,
			center: myLatLng
		});

		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map
		});
	}

</script>
@endsection
