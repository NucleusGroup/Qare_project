@extends('app-front')

@section('content')

<div class="container">





	<div class="row">
		<div class="col-md-8 col-md-offset-2">

			@if($activity)

				<?php
					$user = App\User::find($activity->qProfOIDCreated);
				?>

				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center">
					<h1 class="page-title">{{ $activity->qTitle }}</h1>
					</div>
				</div>

				<a href="{{ url('view-profile/'.$user->qProfOID) }}">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 text-center">
							<div class="image">
								@if($user->qPicture)
									<img src="{{ url('images/users/thumbs/'.$user->qPicture) }}">
								@else
									@if($user->qGender==0 || $user->qGender==1)
										<img src="{{ url('img/user-male.png') }}">
									@else
										<img src="{{ url('img/user-female.png') }}">
									@endif
								@endif
							</div>
						</div>
						<div class="col-md-12 col-xs-12 text-center">
							<h4>{{ $user->qNameFirst.' '.$user->qNameLast }}</h4>
						</div>
					</div>
					</a>

					@if($activity->qProfOIDCreated != Auth::user()->qProfOID)
					<div class="row prev-meta text-center">
						<div class="col-md-6 col-xs-6">
							<?php
								$requestcheck = DB::table('chat')
									->where('qProfOIDRequested', '=', Auth::user()->qProfOID)
									->where('qProfOID', '=', $activity->qProfOIDCreated)
									->where('qStatus', '<>', 2)
									->first();

								$secondrequestcheck = DB::table('chat')
									->where('qProfOID', '=', Auth::user()->qProfOID)
									->where('qProfOIDRequested', '=', $activity->qProfOIDCreated)
									->where('qStatus', '<>', 2)
									->first();

								?>
								@if($requestcheck)
									@if($requestcheck->qStatus == 0)
										<button class="btn btn-info" disabled=""><i class="glyphicon glyphicon-comment"></i> Requested</button>
									@elseif($requestcheck->qStatus == 1)
										<a href="{{ url('messages/'.$user->qProfOID) }}" class="btn btn-success"><i class="glyphicon glyphicon-comment"></i></a>
									@else
										<?php /*<button class="btn btn-danger" disabled=""><i class="glyphicon glyphicon-comment"></i> Declined</button>*/ ?>
									@endif





								@elseif($secondrequestcheck)
									@if($secondrequestcheck->qStatus == 0)
										<button class="btn btn-info" data-toggle="modal" data-target="#chatrequestedit"><i class="glyphicon glyphicon-comment"></i> Confirm</button>

									<div class="modal fade" id="chatrequestedit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
									  <div class="modal-dialog">
									    <div class="modal-content text-center">
										    <div class="modal-header">
										  		<h3>Chat request from {{ $user->qNameFirst.' '.$user->qNameLast }}</h3>
										  	</div>
										  	<div class="modal-body">
										    	<a href="{{ url('chatrequest/'.$user->qProfOID.'/accept') }}" onclick="return confirm('Are you sure you want to accept?')" class="btn btn-success">Accept chat request</a>
										    	<a href="{{ url('chatrequest/'.$user->qProfOID.'/decline') }}" onclick="return confirm('Are you sure you want to decline?')" class="btn btn-danger">Decline chat request</a>
										    </div>
										    <div class="modal-footer">
										        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										      </div>
									    </div>
									  </div>
									</div>
									@elseif($secondrequestcheck->qStatus == 1)
										<a href="{{ url('messages/'.$user->qProfOID) }}" class="btn btn-success"><i class="glyphicon glyphicon-comment"></i></a>
									@else
										<button class="btn btn-primary" data-toggle="modal" data-target="#chatrequest"><i class="glyphicon glyphicon-comment"></i> Request</button>

										<div class="modal fade" id="chatrequest" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
										  <div class="modal-dialog">
										    <div class="modal-content text-center">
											    <div class="modal-header">
											  		<h3>Send chat request to {{ $user->qNameFirst.' '.$user->qNameLast }}</h3>
											  	</div>
											  	<div class="modal-body">
											  		<form method="post" action="{{ url('activities/'.$activity->qActivOID).'/chat' }}">
											  			<input type="hidden" name="_token" value="{{ csrf_token() }}">
											    		<button type="submit" name="send_chat_request" class="btn btn-primary">Send chat request</button>
											    	</form>
											    </div>
											    <div class="modal-footer">
											        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											      </div>
										    </div>
										  </div>
										</div>

										<?php /*<button class="btn btn-danger" disabled=""><i class="glyphicon glyphicon-comment"></i> Declined</button>*/ ?>
									@endif

								@else
									<button class="btn btn-primary" data-toggle="modal" data-target="#chatrequest"><i class="glyphicon glyphicon-comment"></i> Request</button>

									<div class="modal fade" id="chatrequest" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
									  <div class="modal-dialog">
									    <div class="modal-content text-center">
										    <div class="modal-header">
										  		<h3>Send chat request to {{ $user->qNameFirst.' '.$user->qNameLast }}</h3>
										  	</div>
										  	<div class="modal-body">
										  		<form method="post" action="{{ url('activities/'.$activity->qActivOID).'/chat' }}">
										  			<input type="hidden" name="_token" value="{{ csrf_token() }}">
										    		<button type="submit" name="send_chat_request" class="btn btn-primary">Send chat request</button>
										    	</form>
										    </div>
										    <div class="modal-footer">
										        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										      </div>
									    </div>
									  </div>
									</div>
								@endif

						</div>
						<div class="col-md-6 col-xs-6">
							<?php
								$checkifbooked = DB::table('activbookings')
									->where('qActivOID', '=', $activity->qActivOID)
									->where('qProfOIDBookingCreated', '=', Auth::user()->qProfOID)
									->first();
							?>

							@if($checkifbooked)
								<p class="user-details-success">You booked this activity</p>
							@elseif($activity->qProfOIDCreated != Auth::user()->qProfOID)
							<a class="favorite-btn btn-block" href="{{ url('book-activity/'.$activity->qActivOID) }}" onclick="return confirm('Are you sure you want to book this activity?');">Book</a>
							@endif
						</div>
					</div>
				@endif

					@if (session('flash_message'))
				        <div class="alert alert-success">{{ session('flash_message') }}</div>
				    @endif

					@if (session('error_message'))
				        <div class="alert alert-danger">{{ session('error_message') }}</div>
				    @endif

			<div class="row">
				<div class="col-md-8">

					<table class="table table-striped">

					<?php
					if($activity->qLocation != ''){
						$place = explode(' | ', $activity->qLocation);
					?>
					<tr>
						<th>Location</th>
						<td>{{ $place[1] }}</td>
					</tr>
					<?php } ?>
					<tr>
						<th>Date</th>
						<td>{{ date('d.m.Y.', strtotime($activity->qDate)).' '.substr($activity->qTime, 0, -3) }}</td>
					</tr>
					<tr>
						<th>Free spaces</th>
						<td>{{ ($activity->qNofSpaces - $nofbooking).'/'.$activity->qNofSpaces }}</td>
					</tr>
					@if(count($tags) >0)
					<tr>
						<th>Tags</th>
						<td>
							@foreach($tags as $tag)
							<?php
								$qtag = DB::table('envtags')->where('qTagOID', '=', $tag->qTagOID)->where('qIsActive', '=', 1)->first();
							?>
							<p>{{ $qtag->qTagDesc }}</p>
							@endforeach
						</td>
					</tr>
					@endif
					</table>

				</div>
			</div>

			@if($activity->qDescription != '')
			<div class="row">
				<div class="col-md-12">
					<h4>Description</h4>
					<p class="text-justify"> {{ $activity->qDescription }}</p>
				</div>
			</div>
			@endif


		@endif

		</div>
	</div>
</div>

@endsection
