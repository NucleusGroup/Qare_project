@extends('app')

@section('content')

<div class="container">

  <div class="row">
    <div class="col-xs-10 col-xs-offset-1">
      @if (session('flash_message'))
      <div class="alert alert-success">{{ session('flash_message') }}</div>
      @endif
    </div>
  </div>

  <div class="row">
    <div class="col-xs-10 col-xs-offset-1">
      <a class="btn btn-primary pull-right" href="{{ url('admin/activity-add')}}">
        <i class="fa fa-trash"></i> Add activity
      </a>
    </div>
  </div>

  <hr>

  <div class="row">
    <form method="get">
     <div class="col-xs-3 col-xs-offset-1">
      <label for="activity">Activity</label>
      <input type="text" name="activity" class="form-control" value="{{ Request::get('activity') }}">
    </div>
    <div class="col-xs-3">
      <label for="user">User</label>
      <input type="text" name="user" class="form-control" value="{{ Request::get('user') }}">
    </div>
    <div class="col-xs-3 col-xs-offset-1">
      <label for="user">&nbsp;</label><br>
      <input type="submit" class="btn btn-primary" value="Search">
    </div>
  </form>
</div>

<hr>

<div class="row">
<div class="col-md-10 col-md-offset-1">
    <div class="panel panel-default">
      <div class="panel-heading">Activity list</div>

      <div class="panel-body">
        <table class="table table-striped">

          <thead>
            <th>Title</th>
            <th>Created by</th>
            <th>Activity date</th>
            <th>Details</th>
            <th>Created</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
          </thead>

          <tbody>
            @foreach ($activities as $activity)
            <tr>
              <td>
                <div>{{ $activity->qTitle }} </div>
              </td>
              <td>
                <div>{{ App\User::find($activity->qProfOIDCreated)->qNameFirst . ' ' . App\User::find($activity->qProfOIDCreated)->qNameLast }} </div>
              </td>
              <td>
                <div>
                  {{ date('d.m.Y.', strtotime($activity->qDate)) . ' ' . substr($activity->qTime, 0, -3) }}
                </div>
              </td>
              <td>
                <div>
                 <!-- Button trigger modal -->
                 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#activity-{{ $activity->qActivOID }}">
                  View
                </button>

                <!-- Modal -->
                <div class="modal fade" id="activity-{{ $activity->qActivOID }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title" id="myModalLabel"> {{ $activity->qTitle }}</h3>
                      </div>
                      <div class="modal-body">

                        <table class="table table-striped table-condensed">
                          <tr>
                            <th>Created by</th>
                            <td>{{ App\User::find($activity->qProfOIDCreated)->qNameFirst . ' ' . App\User::find($activity->qProfOIDCreated)->qNameLast }}</td>
                          </tr>
                          <tr>
                            <th>Active</th>
                            <td>@if($activity->qIsActive == 0) {{ 'Yes' }} @else {{ 'No' }} @endif</td>
                          </tr>
                          <tr>
                            <th>Public</th>
                            <td>@if($activity->qPublicYN == 1) {{ 'Yes' }} @else {{ 'No' }} @endif</td>
                          </tr>
                          <tr>
                            <th>Outside</th>
                            <td>@if($activity->qOutsideYN == 1) {{ 'Yes' }} @else {{ 'No' }} @endif</td>
                          </tr>

                          @if($activity->qLocation != '')
                          <?php
                          $location = explode(' | ', $activity->qLocation)
                          ?>
                          <tr>
                            <th>Location</th>
                            <td>{{ $location[1] }}</td>
                          </tr>
                          @endif
                          <tr>
                            <th>Date</th>
                            <td>{{ date('d.m.Y. H:i', strtotime($activity->qDate)).' '.substr($activity->qTime, 0, -3) }}</td>
                          </tr>
                          <tr>
                            <th>Available spaces</th>
                            <td>{{ $activity->qNofSpaces }}</td>
                          </tr>
                          <tr>
                            <th>Duration</th>
                            <td>{{ $activity->qDuration }} h</td>
                          </tr>
                          <tr>
                            <th>Price</th>
                            <td>{{ $activity->qPrice }} &#8364;</td>

                        </table>

                        <b>Description: </b>
                        <p>{{ $activity->qDescription }}</p>

                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </td>
            <td>
             <div>{{ date('d.m.Y. H:i', strtotime($activity->qCreatedAt)) }}</div>
           </td>

           <td>
            <a href="{{ url('admin/activity-edit/' . $activity->qActivOID) }}" class="btn btn-info">
             <i class="fa fa-pencil"></i> Update
           </a>
         </td>

         <td>
          <a href="{{ url('admin/activity-delete/' . $activity->qActivOID) }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete activity?')">
           <i class="fa fa-trash"></i> Delete
         </a>

       </td>
     </tr>
     @endforeach
   </tbody>
 </table>
 {!! str_replace('/?', '?', $activities->appends(['activity'=>Input::get('activity'), 'user'=>Input::get('user')])->render()) !!}
</div>

</div>
</div>
</div>
</div>
@endsection
