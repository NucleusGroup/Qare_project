@extends('app')

@section('content')

<div class="container">

	<div class="row">
		<div class="col-md-6 col-md-offset-2 text-center">
			<h3>Add a new activity</h3>
		</div>
	</div>

	@if(count($errors))
	<div class="row">
		<div class="col-md-6 col-md-offset-2 text-center">
			<div class="alert alert-danger">
			@foreach($errors->all() as $error)
				<p>{{ ($error) }}</p>
			@endforeach
			</div>
		</div>
	</div>
	@endif

	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<form method="post" enctype="multipart/form-data" action="{{ url('admin/activity-add') }}">


				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="row">
					<div class="col-md-4">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="active" value="1" checked="checked">
								Active
							</label>
						</div>
					</div>

					<div class="col-md-4">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="public" value="1">
								Public
							</label>
						</div>
					</div>

					<div class="col-md-4">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="outside" value="1">
								Outside
							</label>
						</div>
					</div>
				</div>

				<br>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="title">Profile</label>
							<select name="profile" class="form-control">
								@foreach($users as $user)
								<option value="{{ $user->qProfOID }}">{{ $user->qNameFirst . ' ' .$user->qNameLast }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col-md-8">
						<div class="form-group">
							<label for="title">Title</label>
							<input type="text" class="form-control" name="title" value="{{ old('title') }}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="datetime">Date</label>
							<input type="text" class="form-control datetimepicker" name="datetime" value="@if(old('datetime')) {{ old('datetime') }} @else {{ date('d.m.Y H:i') }} @endif">
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="text">Description</label>
							<textarea class="form-control" name="description" rows="5">{{ old('text') }}</textarea>
						</div>
					</div>
				</div>


				<div class="form-group">
					<div class="row">
						<div class="col-md-8 col-xs-8">
							<label>Location</label>
							<input type="text" id="autocomplete" name="location" value="{{ old('location') }}" onFocus="geolocate()" class="form-control" placeholder="Search for city or zip code">
							<input type="hidden" name="latlon" value="{{ old('latlon') }}" id="longitude">
							<input type="hidden" name="city" value="{{ old('city') }}" id="city">
						</div>
						<div class="col-md-4 col-xs-4">
							<label>Postal code</label>
							<input type="text" id="postal_code" name="postal_code" value="{{ old('postal_code') }}" class="form-control" placeholder="Postal code">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="spaces">Available spaces </label>
							<input type="number" name="spaces" class="form-control" min="1" value="{{ old('spaces') }}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="duration">Duration in hours</label>
							<input type="number" class="form-control" name="duration" min="1" value="{{ old('duration') }}">
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="price">The estimated price</label>
							<input type="number" class="form-control" name="price" min="1" value="{{ old('price') }}">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="locationurl">The URL of the location</label>
							<input type="text" class="form-control" name="locationurl" value="{{ old('locationurl') }}">
						</div>
					</div>
				</div>

				<br>

				<div class="row">
					<div class="col-md-4">
						<input type="submit" class="btn btn-block btn-primary" value="Save">
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

@endsection

@section('tailscripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMAkuIuxWRb2fZ36YTL8a3RKO4tJ6TpGE&libraries=geometry,places&callback=initAutocomplete&language=en" async defer></script>
<script>

	var placeSearch, autocomplete;

	function initAutocomplete() {

		autocomplete = new google.maps.places.Autocomplete(
			(document.getElementById('autocomplete')),
			{types: ['geocode']});

		autocomplete.addListener('place_changed', fillInAddress);
	}

	function fillInAddress() {
		var place = autocomplete.getPlace();
		$("#longitude").val(place.geometry.location.lat() + ', ' + place.geometry.location.lng());


		for (var i = 0; i < place.address_components.length; i++) {

			var addressType = place.address_components[i].types[0];

			if(addressType == 'locality')
			{
				$("#city").val(place.address_components[i].long_name);
			}

			if(addressType == 'postal_code')
			{
				$("#postal_code").val(place.address_components[i].long_name);
			} else {
				$("#postal_code").val('');
			}

		}
	}

	function geolocate() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var geolocation = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				var circle = new google.maps.Circle({
					center: geolocation,
					radius: position.coords.accuracy
				});
				autocomplete.setBounds(circle.getBounds());
			});
		}
	}
</script>
@endsection
