@extends('app-front')

@section('content')

<div class="container">

     <div class="row">
		<div class="col-md-6 col-md-offset-3 text-center">
			<h1 class="page-title">Settings</h1>
		</div>
	</div>

	@if(count($errors))
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
			<p>{{ ($error) }}</p>
		@endforeach
	</div>
	@endif

	@if (session('flash_message'))
          <div class="alert alert-success">{{ session('flash_message') }}</div>
     @endif

	@if (session('error_message'))
          <div class="alert alert-danger">{{ session('error_message') }}</div>
     @endif



     <div class="row">
		<div class="col-md-8 col-md-offset-2">
		<form method="post" action="{{ url('change-email') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			
			<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<label class="control-label">Change email</label>
						</div>
					</div>
				</div>

			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6 col-xs-8">
						<input type="email" class="form-control" name="email" value="{{ $user->qEmail }}">
					</div>
				</div>
			</div>
			
			<br>
			
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="col-md-2">
						<input type="submit" class="btn btn-primary" value="Save">
					</div>
				</div>
			</div>

		</form>
		</div>
	</div>

	<br>
	<br>

     <div class="row">
		<div class="col-md-8 col-md-offset-2">
		<form method="post" action="{{ url('change-password') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<label class="control-label">Change password</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="col-md-6">
							<input type="password" class="form-control" name="oldpassword" placeholder="Enter old password">
						</div>
					</div>
				</div>

				<br>

				<div class="row">
					<div class="col-md-12 text-center">
						<div class="col-md-6">
							<input type="password" class="form-control" name="password" placeholder=" New password">
						</div>
					</div>
				</div>

				<br>

				<div class="row">
					<div class="col-md-12 text-center">
						<div class="col-md-6">
							<input type="password" class="form-control" name="confirmpassword" placeholder="Confirm new password">
						</div>
					</div>
				</div>

				<br>

				<div class="row">
					<div class="col-md-12 text-center">
						<div class="col-md-2">
							<input type="submit" class="btn btn-primary" value="Save">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	<br>
	<br>

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="row">
				<div class="col-md-6 col-xs-6">
					<a href="{{ url('profile') }}" class="btn btn-primary">Add Verifications</a>
				</div>
				<div class="col-md-6 col-xs-6">
					<form method="post" action="{{ url('settings/notification') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<label>Email notification
							<input type="checkbox" name="notification" value="1" @if($user->qNotification==0) {{ 'checked="checked"' }} @endif onclick="this.form.submit()">
						</label>
					</form>
				</div>
			</div>
		</div>
	</div>

	<br>
	<br>


	<br>
	<br>

	<div class="row">
		<div class="col-md-8 col-md-offset-2 dashboard text-center">
			<a href="{{ url('delete-profile') }}" onclick="return confirm('Are you sure you want to delete your profile?')" class="btn btn-danger">Delete profile</a>
		</div>
	</div>
	<div style="padding-bottom:100px;"></div>
</div>

@endsection
