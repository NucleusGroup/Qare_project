<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminQuestions extends Model {

	protected $table = 'questions';

	public $timestamps = false;

}
