@extends('app-front')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="col-md-12 text-center">

				<h1 class="page-title">Help</h1>

			</div>


			<div class="col-md-12 text-justify">
				<h3>Frequently Asked Questions</h3>

				<div class="row">
					<div class="col-md-12 text-justify">
						<h4>Lorem ipsum?</h4>
						<p>Eum ferri persequeris referrentur id, ut sea percipitur reprehendunt. Ancillae nominati mel ut, no accumsan appetere euripidis his. Ut sea vide omittam accusamus, no sit minimum definitiones. Scripta bonorum te eam.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-justify">
						<h4>Lorem ipsum?</h4>
						<p>Eum ferri persequeris referrentur id, ut sea percipitur reprehendunt. Ancillae nominati mel ut, no accumsan appetere euripidis his. Ut sea vide omittam accusamus, no sit minimum definitiones. Scripta bonorum te eam.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-justify">
						<h4>Lorem ipsum?</h4>
						<p>Eum ferri persequeris referrentur id, ut sea percipitur reprehendunt. Ancillae nominati mel ut, no accumsan appetere euripidis his. Ut sea vide omittam accusamus, no sit minimum definitiones. Scripta bonorum te eam.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-justify">
						<h4>Lorem ipsum?</h4>
						<p>Eum ferri persequeris referrentur id, ut sea percipitur reprehendunt. Ancillae nominati mel ut, no accumsan appetere euripidis his. Ut sea vide omittam accusamus, no sit minimum definitiones. Scripta bonorum te eam.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-justify">
						<h4>Lorem ipsum?</h4>
						<p>Eum ferri persequeris referrentur id, ut sea percipitur reprehendunt. Ancillae nominati mel ut, no accumsan appetere euripidis his. Ut sea vide omittam accusamus, no sit minimum definitiones. Scripta bonorum te eam.</p>
					</div>
				</div>

				<br>
				<br>

				<div class="row">
					<div class="col-md-12 text-center dashboard">
						<a href="{{ url('contact') }}" class="btn btn-primary">Contact us now</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

@endsection
