<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use DB;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Mail;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\AdminBookings;
use App\AdminActivity;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;

class FrontController extends Controller {


    /*
    *View for registering a new user
    *If user is logged in, can't see this page. Automaticaly redirect to dashboard
    */
	public function register()
	{
		if(Auth::user()){
            if(Auth::user()->qIsAdmin == 0)
            {
                return redirect('/');
            }
        }

		return view('auth/register');
	}

    /*
    *Registering a new user
    *Input: email, password, confirm password
    *Return: registered user
    *Log in user
    *Send email to user with link for verify profile
    */
	public function registerme(Request $request)
	{
		$validator = Validator::make($request->all(), [
    		'email' => 'required|email',
    		'password' => 'required|min:4',
    		'password_confirmation' => 'required|same:password'
    	]);

    	if($validator->fails())
    	{
    		return redirect('register')
    			->withErrors($validator)
    			->withInput();
    	}

    	$check = DB::table('profiles')
    		->where('qEmail', '=', $request->input('email'))
    		->first();

    	if($check)
    	{
    		Session::flash('error_message', 'Email exists. Try with diferent email address.');

    		return redirect('register')->withInput();
    	}
    	else
    	{
    		$user = new User;

            $emailhash = Hash::make(uniqid());
            $password = $request->input('password');

            $user->qEmail = $request->input('email');
    		$user->qEmailHash = $emailhash;
    		$user->qPassword = Hash::make($request->input('password'));
    		$user->qCreatedAt = date('Y-m-d H:i:s');
            $user->qModifiedAt = date('Y-m-d H:i:s');
    		$user->qDateOfBirth = '0000-00-00';

    		$user->save();

    		Session::flash('flash_message', 'Thank you for registering.');

    		$newuser = DB::table('profiles')
    			->where('qEmail', '=', $request->input('email'))
    			->where('qIsAdmin', '=', 0)
    			->where('qIsDeleted', '=', 0)
    			->first();

            Mail::send('emails.register', compact('newuser', 'emailhash', 'password'), function($message) use ($newuser)
            {
                $message->from('qare-project@qare.com', 'Qare project');
                $message->to($newuser->qEmail, $newuser->qNameFirst.' '.$newuser->qNameLast)->subject('Registration - Qare project');
            });

    		if (Auth::attempt([
    			'qEmail' => $request->input('email'),
    			'password' => $request->input('password'),
    			'qIsAdmin' => 0,
                'qIsDeleted' => 0
    		], $request->has('remember')))
    		{
    			return redirect('/dashboard');
    		}

    	}

	}

    /*
    *Reset password first step
    *Input: email
    *Send email to user with link for page where user can edit password
    */
	public function resetpasswordemail(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'required|email'
		]);

		if($validator->fails())
		{
			return redirect('resetpassword')
    			->withErrors($validator)
    			->withInput();
		}

		$checkifexist = DB::table('profiles')
			->where('qEmail', '=', $request->input('email'))
			->where('qIsAdmin', '=', 0)
			->where('qIsDeleted', '=', 0)
			->first();

		if($checkifexist)
		{
			$token = str_random(64);

			$user = User::find($checkifexist->qProfOID);
			$user->qRememberToken = $token;
			$user->save();

			Session::flash('flash_message', 'We sent you link for reseting password to your email address.');

            Mail::send('emails.password', ['token' => $token], function($message) use ($request, $checkifexist)
            {
                $message->from('qare-project@qare.com', 'Qare project');
                $message->to($request->input('email'), $checkifexist->qNameFirst.' '.$checkifexist->qNameLast)->subject('Reset password - Qare project');

            });


			return redirect('login');
		}

        Session::flash('error_message', 'Something is wrong. Please try again.');

        return redirect('login');
	}


    /*
    *Reset password
    *Input: new password, confirm new password
    *Return: edit profile password
    *Send email to user with new password
    */
	public function resetpassword(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'required|email',
			'password' => 'required|min:4',
			'password_confirmation' => 'required|same:password'
		]);

		if($validator->fails())
		{
			return redirect('resetpassword')
    			->withErrors($validator)
    			->withInput();
		}

		$checkifexist = DB::table('profiles')
			->where('qEmail', '=', $request->input('email'))
			->where('qIsAdmin', '=', 0)
			->where('qIsDeleted', '=', 0)
			->first();

		if($checkifexist)
		{

			$user = User::find($checkifexist->qProfOID);
			$user->qPassword = Hash::make($request->input('password'));
			$user->save();

			Session::flash('flash_message', 'You successfully reset your password.');

            Mail::send('emails.newpassword', ['password' => $request->input('password')], function($message) use ($request, $checkifexist)
            {
                $message->from('qare-project@qare.com', 'Qare project');
                $message->to($checkifexist->qEmail, $checkifexist->qNameFirst.' '.$checkifexist->qNameLast)->subject('New password - Qare project');
            });

			return redirect('login');
		}
	}


    /*
    *Log in view
    *If user is logged in, redirect to dashboard
    */
	public function login()
	{

        if(Auth::user()){
            if(Auth::user()->qIsAdmin == 0)
            {
                return redirect('/');
            }
        }

		return view('login');
	}


    /*
    *Log in user
    *Input: email and password
    *Return logged in user
    */
	public function loginme(Request $request)
	{
		$validator = Validator::make($request->all(), [
    		'email' => 'required|email',
    		'password' => 'required',
    	]);

    	if($validator->fails())
    	{
    		return redirect('login')
    			->withErrors($validator)
    			->withInput();
    	}

        if($request->input('remember') == 1){ $remember = 1;} else {$remember = null;}

    	if (Auth::attempt([
    			'qEmail' => $request->input('email'),
    			'password' => $request->input('password'),
    			'qIsAdmin' => 0,
                'qIsDeleted' => 0
    	], $remember))
    	{
    		return redirect('/');

    	} else {
    		Session::flash('error_message', 'Wrong credentials');

    		return redirect('login')->withInput();

    	}

	}


    /*
    *Facebook with permission for email for facebook user
    */
	public function facebook()
	{
	    return Socialite::with('facebook')->scopes(['email'])->redirect();
	}


    /*
    *Facebook callback
    *Callback return: facebook profile
    *Return: new user in database if email don't exist
    *Log in user
    */
	public function callback()
	{
	    $fbuser = Socialite::with('facebook')->scopes(['email'])->user();

		$check = DB::table('profiles')
	    	->where('qEmail', '=', $fbuser->user['email'])
	    	->first();

	    if($check)
	    {
	    	Auth::loginUsingId($check->qProfOID);

	    	return redirect('/dashboard');
	    }
	    else
	    {
	    	$user = new User;

            $emailhash = Hash::make(uniqid());

            $fullname = explode(' ', $fbuser->user['name']);
            $name = $fullname[0];
            $lastname = end($fullname);

            $user->qEmail = $fbuser->user['email'];
	    	$user->qEmailHash = $emailhash;
            $user->qNameFirst = $name;
	    	$user->qNameLast = $lastname;
            $user->qDateOfBirth = '0000-00-00';

	    	if(isset($fbuser->user['gender'])  && $fbuser->user['gender'] =='male')
	    	{
	    		$user->qGender = 1;
	    	} else if(isset($fbuser->user['gender'])  && $fbuser->user['gender']=='female')
	    	{
	    		$user->qGender = 2;
	    	} else {
	    		$user->qGender = 0;
	    	}
	    	$image = uniqid().'.jpg';
	    	copy($fbuser->avatar_original, public_path().'/images/users/'.$image);
	    	copy($fbuser->avatar, public_path().'/images/users/thumbs/'.$image);

	    	$user->qPicture = $image;
	    	$user->qCreatedAt = date('Y-m-d H:i:s');
    		$user->qModifiedAt = date('Y-m-d H:i:s');

	    	$user->save();

	    	$login = DB::table('profiles')
		    	->where('qEmail', '=', $fbuser->user['email'])
		    	->first();

	    	Auth::loginUsingId($login->qProfOID);

            Mail::send('emails.fbregister', compact('fbuser', 'emailhash'), function($message) use ($fbuser)
            {
                $message->from('qare-project@qare.com', 'Qare project');
                $message->to($fbuser->user['email'], $fbuser->user['name'])->subject('Registration - Qare project');
            });


	    	return redirect('/dashboard');
	    }

	    return redirect('login');
	}


    /*
    *Logout user
    */
    public function logout()
    {
        Auth::logout();

        return redirect('login');
    }


    /*
    *Dashboard page for logged in user
    */
    public function dashboard()
    {
    	$user = Auth::user();

        $user2 = User::find(Auth::user()->qProfOID);


        $myinvites = DB::table('activbookings')->select('activbookings.qActivOID', 'activbookings.qProfOIDBookingCreated', 'activbookings.qStatus', 'activbookings.qCreatedAt')
                        ->join('activities', 'activities.qActivOID', '=', 'activbookings.qActivOID')
                        ->join('profiles', 'activities.qProfOIDCreated', '=', 'profiles.qProfOID')
                        ->where('profiles.qProfOID', '=', $user2->qProfOID)
                        ->orderBy('qStatus', 'ASC')
                        ->get();

        $percentage = 0;

        if ( $user->qPicture != "") $percentage += 10;
        if ( $user->qNameFirst != "") $percentage += 10;
        if ( $user->qNameLast != "") $percentage += 10;
        if ( $user->qDateOfBirth != "") $percentage += 10;
        if ( $user->qAddress != "") $percentage += 10;
        if ( $user->qZipCode != "") $percentage += 10;
        if ( $user->qCity != "") $percentage += 10;
        if ( $user->qPhoneNumber != "") $percentage += 10;
        if ( $user->qDescription != "") $percentage += 10;
        if ( $user->qInterest != "") $percentage += 10;

    	return view('dashboard', compact('user'))->with('percentage', $percentage)->with('myinvites', $myinvites);
    }

    /*
    *Static page "How it works"
    */
    public function howitworks()
    {
    	return view('how-it-works');
    }


    /*
    *Static page "Terms and conditions"
    */
    public function termsandconditions()
    {
    	return view('terms-and-conditions');
    }


    /*
    *Static page "Help"
    */
    public function help()
    {
    	return view('help');
    }


    /*
    *Static page "Contact"
    */
    public function contact()
    {
    	return view('contact');
    }


    /*
    *Contact administrator
    *Send email to administrator with user message
    */
    public function contactus(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    		'name' => 'required|min:3',
    		'email' => 'required|email',
    		'title' => 'required|min:3',
    		'message' => 'required|min:3'
    	]);

    	if($validator->fails())
    	{
    		return redirect('/')
    			->withErrors($validator)
    			->withInput();
    	}

    	Session::flash('flash_message', 'Thank you for contacting us. We well answer you as soon as possible.');

    	Mail::raw('Contact: '.$request->input('message'), function($message) use ($request)
    	{
    		$message->from($request->input('email'), $request->input('name'))->subject('Contact form - Qare project');
    		$message->to('qare-project@qare.com');
    	});

    	return view('welcome');
    }

    /*
    *Email verification
    *Input: email hash from link
    *Return: verified email
    */
    public function verifyemail($emailhash)
    {
        $check = DB::table('profiles')
            ->where('qEmailHash', '=', $emailhash)
            ->where('qEmailVerifiedYN' ,'=', 0)
            ->first();

        if($check)
        {
            DB::table('profiles')
            ->where('qEmailHash', '=', $emailhash)
            ->where('qEmailVerifiedYN' ,'=', 0)
            ->update([
                'qEmailVerifiedYN' => 1
            ]);

            Session::flash('flash_message', 'Thank you for verifying email address.');

            if(Auth::user()){
                return redirect('dashboard');
            }
            else {
                return redirect('login');
            }

        }
        else {

            Session::flash('error_message', 'Something is wrong.');

            if(Auth::user()){
                return redirect('dashboard');
            }
            else {
                return redirect('login');
            }
        }
    }

}
