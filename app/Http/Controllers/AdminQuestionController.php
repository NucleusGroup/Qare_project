<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use App\AdminQuestions;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;

class AdminQuestionController extends Controller {

	public function index()
	{
		if(Input::get('question') || Input::get('user'))
		{
			$questions = DB::table('questions')
				->whereRaw("LOWER(questions.questionTitle) LIKE '%".strtolower(Input::get('question'))."%'")
				->where('parentId', '=', 0)
	            ->join('profiles', 'profiles.qProfOID', '=', 'questions.user_id')
	            ->whereRaw("CONCAT (LOWER(profiles.qNameFirst), ' ', LOWER(profiles.qNameLast)) LIKE '%".strtolower(Input::get('user'))."%'")
	            ->select('questions.*')
	            ->paginate(50);
	    } else {
	        $questions = DB::table('questions')->where('parentId', '=', 0)->paginate(50);
	    }


		return view('admin.questions', compact('questions'));
	}

	public function addQuestion()
	{
		$users = DB::table('profiles')->where('qIsAdmin', '=', 0)->where('qIsDeleted', '=', 0)->get();
		return view('admin.question-add', compact('users'));
	}

	public function addNewQuestion(Request $request)
	{
		$question = new AdminQuestions;

		$validator = Validator::make($request->all(), [
			'title' => 'required|min:3',
			'question' => 'required|min:3'
		]);

		if($validator->fails())
		{
			return redirect('admin/question-add')
				->withErrors($validator)
				->withInput();
		}

		if($request->input('active') == 1)
		{
			$question->active = 1;
		} else {
			$question->active = 0;
		}

		$question->questionTitle = $request->input('title');
		$question->questionText = $request->input('question');
		$question->user_id = $request->input('profile');
		$question->modifiedAt = date('Y-m-d H:i:s');

		$question->save();

		Session::flash('flash_message', 'You successfully added question');

		return redirect('admin/questions');
	}


	public function addAnswer($id, Request $request)
	{

		$parent = DB::table('questions')->where('id', '=', $id)->first();
		$user = DB::table('profiles')->where('qProfOID', '=', $parent->user_id)->first();
		$child = DB::table('questions')->where('parentId', '=', $id)->first();

		if($child)
		{
			$child = AdminQuestions::where('parentId', '=', $id)->first();
		} else {
			$child = new AdminQuestions;
		}

		$child->questionText = $request->input('question');
		$child->parentId = $id;
		$child->user_id = Auth::user()->qProfOID;
		$child->modifiedAt = date('Y-m-d H:i:s');

		$child->save();

		Mail::send('emails.question', compact('parent', 'child'), function($message) use ($user)
        {
            $message->from('qare-project@qare.com', 'Qare project');
            $message->to($user->qEmail)->subject('Qare project - question');
        });

		Session::flash('flash_message', 'You successfully answred to question');

		return redirect('admin/questions');
	}

	public function editQuestion($id)
	{
		$question = AdminQuestions::find($id);
		$users = DB::table('profiles')->where('qIsAdmin', '=', 0)->get();

		return view('admin.question-edit', compact('question', 'users'));
	}

	public function editThisQuestion($id, Request $request)
	{
		$question = AdminQuestions::find($id);

		$validator = Validator::make($request->all(), [
			'title' => 'required|min:3',
			'question' => 'required|min:3'
		]);

		if($validator->fails())
		{
			return redirect('admin/question-edit/'.$id)
				->withErrors($validator)
				->withInput();
		}


		$question->questionTitle = $request->input('title');
		$question->questionText = $request->input('question');
		$question->modifiedAt = date('Y-m-d H:i:s');

		$question->save();

		Session::flash('flash_message', 'You successfully edited question');

        return redirect('admin/questions');

	}


	public function deleteQuestion($id)
	{
		$bookings = AdminQuestions::where('id', '=', $id)->orwhere('parentId', '=', $id)->delete();

		Session::flash('flash_message', 'Question has been successfully deleted');

		return redirect('admin/questions');
	}

}
