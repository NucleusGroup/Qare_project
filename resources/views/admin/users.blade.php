@extends('app')

@section('content')

<div class="container">

    <div class="row">
      <div class="col-xs-10 col-xs-offset-1">
        @if (session('flash_message'))
          <div class="alert alert-success">{{ session('flash_message') }}</div>
        @endif
      </div>
    </div>

    @if(count($errors))
    <div class="row">
      <div class="col-md-6 col-md-offset-2 text-center">
        <div class="alert alert-danger">
          @foreach($errors->all() as $error)
          <p>{{ ($error) }}</p>
          @endforeach
        </div>
      </div>
    </div>
    @endif

    <div class="row">
      <div class="col-xs-10 col-xs-offset-1">
            <a class="btn btn-primary pull-right" href="{{ url('admin/user-add')}}">
              <i class="fa fa-trash"></i> Add User
            </a>
      </div>
    </div>

    <hr>

    <div class="row">
    <form method="get">
     <div class="col-xs-3 col-xs-offset-1">
      <label for="user">Profile</label>
      <input type="text" name="user" class="form-control" value="{{ Request::get('user') }}">
     </div>

     <div class="col-xs-3 col-xs-offset-1">
      <label for="user">&nbsp;</label><br>
    <input type="submit" class="btn btn-primary" value="Search">
     </div>
    </form>
  </div>

  <hr>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">User list</div>

                  <div class="panel-body">
                    <table class="table table-striped">

                      <thead>
                        <th>User</th>
                        <th>Email</th>
                        <th>Admin</th>
                        <th>Details</th>
                        <th>Created</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                      </thead>

                      <tbody>
                        @foreach ($users as $user)
                          <tr>
                            <td>
                              <div>{{ $user->qNameFirst }}</div>
                            </td>
                            <td>
                              <div>{{ $user->qEmail }}</div>
                            </td>
                            <td>
                              <div>
                                @if($user->qIsAdmin == 1){{ 'Yes' }} @else {{ 'No' }} @endif
                              </div>
                            </td>
                            <td>
                              <div>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#user-{{ $user->qProfOID }}">
                                  View
                                </button>
                               <div class="modal fade" id="user-{{ $user->qProfOID }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                               <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel"> {{ $user->qNameFirst.' '.$user->qNameLast }}</h4>
                                  </div>
                                  <div class="modal-body">

                                  <div class="row">
                                    <div class="col-md-6">
                                    <table class="table table-striped table-condensed">
                                      <tr>
                                        <th>Profile ID</th>
                                        <td>{{ $user->qProfOID }}</td>
                                      </tr>
                                      <tr>
                                        <th>Created at</th>
                                        <td>{{ date('d.m.Y. H:i:s', strtotime($user->qCreatedAt)) }}</td>
                                      </tr>
                                      <tr>
                                        <th>Modified at</th>
                                        <td>{{ date('d.m.Y. H:i:s', strtotime($user->qModifiedAt)) }}</td>
                                      </tr>
                                      <tr>
                                        <th>User is admin</th>
                                        <td>
                                          @if($user->qIsAdmin == 1)
                                            {{ 'Yes' }}
                                          @else
                                            {{ 'No' }}
                                          @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Email</th>
                                        <td>{{ $user->qEmail }}</td>
                                      </tr>
                                      <tr>
                                        <th>First name</th>
                                        <td>{{ $user->qNameFirst }}</td>
                                      </tr>
                                      <tr>
                                        <th>Name prefix</th>
                                        <td>{{ $user->qNamePrefix }}</td>
                                      </tr>
                                      <tr>
                                        <th>First name</th>
                                        <td>{{ $user->qNameLast }}</td>
                                      </tr>
                                      <tr>
                                        <th>Date of birth</th>
                                        <td>{{ date('d.m.Y.', strtotime($user->qDateOfBirth)) }}</td>
                                      </tr>
                                      <tr>
                                        <th>Gender</th>
                                        <td>
                                          @if($user->qGender == 0)
                                            {{ 'Not applicable' }}
                                          @elseif($user->qGender == 1)
                                            {{ 'Male' }}
                                          @else
                                            {{ 'Female' }}
                                          @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Address</th>
                                        <td>{{ $user->qAddress }}</td>
                                      </tr>
                                      <tr>
                                        <th>Zip code</th>
                                        <td>{{ $user->qZipCode }}</td>
                                      </tr>
                                      <tr>
                                        <th>City</th>
                                        <td>{{ $user->qCity }}</td>
                                      </tr>
                                      <tr>
                                        <th>Phone number</th>
                                        <td>{{ $user->qPhoneNumber }}</td>
                                      </tr>
                                      <tr>
                                        <th>Facebook url</th>
                                        <td>{{ $user->qURLFacebook }}</td>
                                      </tr>
                                      <tr>
                                        <th>Twitter url</th>
                                        <td>{{ $user->qURLTwitter }}</td>
                                      </tr>
                                      <tr>
                                        <th>LinkedIn url</th>
                                        <td>{{ $user->qURLLinkedIn }}</td>
                                      </tr>
                                      <tr>
                                        <th>Instagram url</th>
                                        <td>{{ $user->qURLInstagram }}</td>
                                      </tr>
                                      <tr>
                                        <th>Other url</th>
                                        <td>{{ $user->qURLOther }}</td>
                                      </tr>
                                      <tr>
                                        <th>Email addres of friend</th>
                                        <td>{{ $user->qEmailFriend }}</td>
                                      </tr>
                                      <tr>
                                        <th>Member level</th>
                                        <td>
                                          @if($user->qProfileMemberLevel == 0)
                                            {{ 'Light member' }}
                                          @else
                                            {{ 'Full member' }}
                                          @endif
                                        </td>
                                      </tr>
                                      </table>
                                  </div>
                                  <div class="col-md-6">
                                    <table class="table table-striped table-condensed">

                                      <tr>
                                        <th>Interest</th>
                                        <td>
                                          {{ $user->qInterest }}
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Verified email address</th>
                                        <td>
                                            @if($user->qEmailVerifiedYN==1) {{ 'Yes' }} @else {{ 'No' }} @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Verified phone number</th>
                                        <td>
                                            @if($user->qPhoneVerifiedYN==1) {{ 'Yes' }} @else {{ 'No' }} @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Verified social media</th>
                                        <td>
                                            @if($user->qSocialMediaVerifiedYN==1) {{ 'Yes' }} @else {{ 'No' }} @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Verified ID</th>
                                        <td>
                                            @if($user->qIDVerifiedYN==1) {{ 'Yes' }} @else {{ 'No' }} @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Verified VOG</th>
                                        <td>
                                            @if($user->qVOGVerifiedYN==1) {{ 'Yes' }} @else {{ 'No' }} @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Default preference for Male in searches</th>
                                        <td>
                                            @if($user->qPrefMaleYN==1) {{ 'Yes' }} @else {{ 'No' }} @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Default preference for Female in searches</th>
                                        <td>
                                            @if($user->qPrefFemaleYN==1) {{ 'Yes' }} @else {{ 'No' }} @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Default preference for Inside in searches</th>
                                        <td>
                                            @if($user->qPrefInsideYN==1) {{ 'Yes' }} @else {{ 'No' }} @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Default preference for Outside in searches</th>
                                        <td>
                                            @if($user->qPrefOutsideYN==1) {{ 'Yes' }} @else {{ 'No' }} @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Default preference for Individual in searches</th>
                                        <td>
                                            @if($user->qPrefIndividualYN==1) {{ 'Yes' }} @else {{ 'No' }} @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Default preference for Group in searches</th>
                                        <td>
                                            @if($user->qPrefGroupYN==1) {{ 'Yes' }} @else {{ 'No' }} @endif
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Default distance in KM in searches</th>
                                        <td>
                                            {{ $user->qPrefDistance }}
                                        </td>
                                      </tr>
                                      <tr>
                                        <th>Picture</th>
                                        <td>
                                          @if($user->qPicture)
                                            <img src="{{ url('images/users/thumbs/' . $user->qPicture) }}" height="100">
                                          @endif
                                        </td>
                                      </tr>
                                    </table>
                                    </div>
                                    <div class="col-md-12">
                                        <h4>Description</h4>
                                        <p>
                                          {{ $user->qDescription }}
                                        </p>
                                    </div>
                                    @if($user->qInterest != '')
                                    <div class="col-md-12">
                                        <h4>Interest</h4>
                                        <p>
                                          {{ $user->qInterest }}
                                        </p>
                                    </div>
                                    @endif
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                              </div>
                            </td>
                            <td>
                             <div>{{ date('d.m.Y. H:i', strtotime($user->qCreatedAt)) }}</div>
                            </td>

                            <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#email-{{ $user->qProfOID }}">
                              <i class="glyphicon glyphicon-envelope"></i>
                            </button>

                            <div class="modal fade" id="email-{{ $user->qProfOID }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="exampleModalLabel">New message</h4>
                                  </div>
                                  <form method="post" action="{{ url('admin/sendemail') }}">
                                  <div class="modal-body">
                                      <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                      <div class="form-group">
                                        <label for="recipient-name" class="control-label">Recipient:</label>
                                        <input type="email" class="form-control" id="recipient-name" name="email" value="{{ $user->qEmail }}">
                                      </div>
                                      <div class="form-group">
                                        <label for="message-text" class="control-label">Message:</label>
                                        <textarea class="form-control" id="message-text" name="message"></textarea>
                                      </div>

                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Send email</button>
                                  </form>
                                  </div>
                                </div>
                              </div>
                            </div>

                            </td>
                            <td>
                                <a href="{{ url('admin/users-edit/' . $user->qProfOID) }}" class="btn btn-info">
                                   <i class="fa fa-pencil"></i> Update
                                </a>
                            </td>

                            <td>
                              <a href="{{ url('admin/users-delete/' . $user->qProfOID) }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete user?')">
                                   <i class="fa fa-trash"></i> Delete
                                </a>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                     </table>

                     {!! str_replace('/?', '?', $users->appends(['user'=>Input::get('user')])->render()) !!}
                  </div>

            </div>
        </div>
    </div>
</div>
@endsection
