@extends('app-front')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 text-center">
			<h1  class="page-title zoekenact">Zoeken</h1>
		</div>
	</div>


	<div class="row">

			<div class="edit-form">

			<form method="get" action="{{ url('activities') }}" class="search-form zoekenactform">

				<div class="form-group">
						<input type="text" id="autocomplete" name="location" value="{{ old('location') }}" onFocus="geolocate()" class="form-control" placeholder="STAD EN ZIP CODE">
						<input type="hidden" name="latlon" value="{{ old('latlon') }}" id="longitude">
						<input type="hidden" name="city" value="{{ old('city') }}" id="city">
				</div>
				<div class="form-group kiesafstand">
						<p class="kies">KIES DE AFSTAND</p>
						<span class="kmlimit">100 KM</span>
						<input id="distance" type="text" name="distance" value="{{ old('distance') }}" data-slider-handle="round" data-slider-tooltip="always">
						<!-- <input type="number" name="distance" value="{{ old('distance') }}" min="0" class="form-control"> -->
				</div>

				<div class="form-group">
					<input type="text" name="date" id="date" class="form-control" placeholder="DATUM">
				</div>

				<div class="form-group">
					<p class="kies">SELECTEER TAGS</p>
					<select data-placeholder="Selecteer tags" class="form-control chosen-select" multiple name="tags[]">
						<option value=""></option>
						@if(count($tags))
							@foreach($tags as $tag)
								<?php
									$tagname = DB::table('envtags')->where('qTagOID', '=', $tag->qTagOID)->first();
								?>
								<option value="{{ $tagname->qTagOID }}">{{ $tagname->qTagDesc }}</option>
							@endforeach
						@endif
					</select>
				</div>
				
				<p class="kies">PREFERENCES</p>
				<div class="row custom-checkbox customcb">

					<div>
						<input type="checkbox" id="home" class="checkbox-inline" name="home" value="1">
						<label for="home"><span></span> Home</label>
					</div>
					<div>
						<input type="checkbox" id="inside" class="checkbox-inline" name="inside" value="1">
						<label for="inside"><span></span> Inside</label>
					</div>

					<div>
						<input type="checkbox" id="public" class="checkbox-inline" name="public" value="1">
						<label for="public"><span></span> Public</label>
					</div>

					<div>
						<input type="checkbox" id="outside" class="checkbox-inline" name="outside" value="1">
						<label for="outside"><span></span> Outside</label>
					</div>


				</div>

				
				
				<p class="kies">SELECTEER DAGEN</p>
				<div class="row custom-checkbox customcb">

					<div>
						<input type="checkbox" id="monday" class="checkbox-inline" name="days[]" value="2">
						<label for="monday"><span></span> Monday</label>
					</div>

					<div>
						<input type="checkbox" id="tuesday" class="checkbox-inline" name="days[]" value="3">
						<label for="tuesday"><span></span> Tuesday</label>
					</div>

					<div>
						<input type="checkbox" id="wednesday" class="checkbox-inline" name="days[]" value="4">
						<label for="wednesday"><span></span> Wednesday</label>
					</div>

					<div>
						<input type="checkbox" id="thursday" class="checkbox-inline" name="days[]" value="5">
						<label for="thursday"><span></span> Thursday</label>
					</div>

					<div>
						<input type="checkbox" id="friday" class="checkbox-inline" name="days[]" value="6">
						<label for="friday"><span></span> Friday</label>
					</div>

					<div>
						<input type="checkbox" id="saturday" class="checkbox-inline" name="days[]" value="7">
						<label for="saturday"><span></span> Saturday</label>
					</div>

					<div>
						<input type="checkbox" id="sunday" class="checkbox-inline" name="days[]" value="1">
						<label for="sunday"><span></span> Sunday</label>
					</div>

				</div>

				<br>

				<div class="row">
	                <div class="col-md-12">
	                  <button type="submit" class="btn btn-info btn-premium btn-block"><i class="btn glyphicon glyphicon-search"></i> PLAATS OPROEP</button>
	                </div>
              	</div>

			</form>
			</div> <!-- /.edit-form -->

	</div>

	<br/>
</div>
@endsection

@section('tailscripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMAkuIuxWRb2fZ36YTL8a3RKO4tJ6TpGE&libraries=geometry,places&callback=initAutocomplete&language=en" async defer></script>
<script>

	var slider = new Slider("#distance", {
		step: 5,
		min: 0,
		max: 100,
		formatter: function(value) {
            return value + ' KM';
          },
		tooltip_position: 'top'
	});

	var placeSearch, autocomplete;

	function initAutocomplete() {

		autocomplete = new google.maps.places.Autocomplete(
			(document.getElementById('autocomplete')),
			{types: ['geocode']});

		autocomplete.addListener('place_changed', fillInAddress);
	}

	function fillInAddress() {
		var place = autocomplete.getPlace();
		$("#longitude").val(place.geometry.location.lat() + ', ' + place.geometry.location.lng());


		for (var i = 0; i < place.address_components.length; i++) {

			var addressType = place.address_components[i].types[0];

			if(addressType == 'locality')
			{
				$("#city").val(place.address_components[i].long_name);
			}

		}
	}

	function geolocate() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var geolocation = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
				var circle = new google.maps.Circle({
					center: geolocation,
					radius: position.coords.accuracy
				});
				autocomplete.setBounds(circle.getBounds());
			});
		}
	}
</script>
<script type="text/javascript">

	$(document).ready(function(){

		if($("#date").val().length == 0)
		{
			$(".days").find('input').removeAttr('disabled');
		}
	});

	$('#date').datepicker({
	    dateFormat: 'dd.mm.yy',
	    onSelect: function(dateText){
	    	$(".days").find('input').attr('disabled', 'disabled');
	    }
	});

	$("#date").change(function(){
		if($("#date").val().length == 0)
		{
			$(".days").find('input').removeAttr('disabled');
		}
	});

    $(".chosen-select").chosen({
     	no_results_text: "Oops, nothing found!",
     	max_selected_options: 5
    });
</script>
@endsection
