<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Qare project</title>

	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('/css/jquery-ui-timepicker-addon.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/fileinput.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/stylesheet.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/chosen.css') }}">
	<link rel="stylesheet" href="{{ asset('/css/bootstrap-slider.css') }}">
	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/app2.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/app-responsive.css') }}" rel="stylesheet">


	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<?php 
$nogo = false;
if ( $pageSlug == '' || $pageSlug == 'login' || $pageSlug == 'register' || $pageSlug == 'contact' || $pageSlug == 'how-does-it-work') $nogo = true;
?>

<body class="{{ $pageSlug }}">
	<div class="container">
	    <div class="bearmask"></div>
	</div> <!-- /.container -->
	@if ( ! $nogo )
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				@if(Auth::user() && Auth::user()->qIsAdmin == 0)
				<?php
					$counter = DB::table('chatmessages')
					->where('qProfOIDReceiver', '=', Auth::user()->qProfOID)
					->where('qSeen', '=', 0)
					->groupBy('qChatOID')
					->get();

					if(count($counter) > 0)
					{
						$returnhtml = '<i class="glyphicon glyphicon-envelope"></i> <span class="badge">'.count($counter).'</span>';
					} else {
						$returnhtml = '<i class="glyphicon glyphicon-envelope"></i>';
					}
				?>
				<a class="navbar-brand pull-right" id="chat-messages-envelope-responsive" href="{{ url('messages') }}">{!! $returnhtml !!}</a>
				@endif
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav bear">
            	<li><a><img src="{{ url('/images/bear.png') }}"></a></li>
            </ul>
			@if (Auth::user() && Auth::user()->qIsAdmin == 0)
				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/') }}">Home</a></li>
						<li><a href="{{ url('how-does-it-works') }}">How it works</a></li>
						<li><a href="{{ url('help') }}">Help</a></li>
						<li><a href="{{ url('contact') }}">Contact</a></li>
						<li><a href="{{ url('/login') }}">Login</a></li>
					@elseif(Auth::user()->qIsAdmin == 0)

						<!-- <li><a href="#" id="notification-icon-navbar"><i class="glyphicon glyphicon-info-sign"></i></a></li> -->
						<!-- <li><a href="{{ url('messages') }}" id="chat-messages-envelope">{!! $returnhtml !!}</a></li> -->
						<?php 
						$name = 'mijn account';
						if (Auth::user()->qNameFirst != "") {
							$name = Auth::user()->qNameFirst;
						}
						?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ $name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/view-profile') }}/{{ Auth::user()->qProfOID }}"><i class="glyphicon glyphicon-user"></i> mijn profiel</a></li>
								<li><a href="{{ url('/settings') }}"><i class="glyphicon glyphicon-cog"></i> settings</a></li>
								<li><a href="{{ url('/logout') }}"><i class="glyphicon glyphicon-off"></i> log uit</a></li>
							</ul>
						</li>
					@endif
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{ url('/dashboard') }}">mijn dashboard</a></li>
					<li><a href="{{ url('/activities') }}">activities</a></li>
					<li><a href="{{ url('/profiles') }}">profiles</a></li>
					<li><a href="{{ url('/invites') }}">invites</a></li>
					<li><a href="{{ url('/favorites') }}">favorieten</a></li>
					<li><a href="{{ url('/add-activity') }}">add new activity</a></li>
					
				</ul>
				
			@endif
				
					@if (Auth::guest())

				<ul class="nav navbar-nav navbar-right">
						<li><a href="{{ url('/') }}">Home</a></li>
						<li><a href="{{ url('how-does-it-works') }}">How it works</a></li>
						<li><a href="{{ url('help') }}">Help</a></li>
						<li><a href="{{ url('contact') }}">Contact</a></li>
						<li><a href="{{ url('/login') }}">Login</a></li>
				</ul>
					@endif
			</div>
		</div>
	</nav>
	@endif

	@yield('content')

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="{{ asset('/js/jquery-ui-timepicker-addon.js') }}"></script>
	<script src="{{ asset('/js/fileinput.js') }}"></script>
	<script src="{{ asset('/js/chosen.jquery.js') }}"></script>
	<script src="{{ asset('/js/bootstrap-slider.js') }}"></script>
	<script src="{{ asset('/js/custom.js') }}"></script>

	@yield('tailscripts')

	<script>
	@if (Auth::guest())

	@elseif(Auth::user()->qIsAdmin == 0)

	$(document).ready(function(){
		// pullCountMessages();
	});

	function pullCountMessages()
	{
		retrieveCountChatMessages();
		setTimeout(pullCountMessages,3000);
	}

	function retrieveCountChatMessages()
	{
		$.ajax({
			url: '{{ url('chat/retrieveCountChatMessages') }}',
			type: 'get',
			data: {
				'user'   : {{ Auth::user()->qProfOID }}
			},
			success: function(response) {
				if(response.length > 0){
					//$("#chat-messages-envelope").html(response);
					$("#chat-messages-envelope-responsive").html(response);
				}
			}
		});
	}

	@endif

	  $(function() {

	    $(".datetimepicker").datetimepicker({
	    	changeMonth: true,
    		changeYear: true,
	    	dateFormat: 'dd.mm.yy.',
	    	timeFormat:  "HH:mm"
	    }).datetimepicker("setDate", new Date());

	    $(".timepicker").datetimepicker({
	    	dateFormat: false,
	    	timeFormat:  "HH:mm",
	    	timeOnly: true
	    });

	    $(".date").datepicker({
	    	changeMonth: true,
    		changeYear: true,
    		yearRange: "-100:+0",
	    	dateFormat: 'dd.mm.yy.'
	    });

	    $('body').on('click', 'button.log-in-btn', function(e) {
	    	e.preventDefault();
	    	$('li.login a')[0].click();
	    });

	    $('body').on('click', 'button.log-out-btn', function(e) {
	    	e.preventDefault();
	    	$('li.logout a')[0].click();
	    });

	    if ($('.tcb-quote-carousel').length) {
	    	if ( $('.tcb-quote-carousel .item.row').length < 5) {
	    		$('.carousel-indicators').hide();
	    	}

	    }

	    if ($('.overlay-right.profiel').length) {
	    	var h = $('.overlay-right.profiel').html();
	    	$('.maakmsgprofiel').html(h);
	    	$('.maakmsgprofiel .orangebtn').insertBefore('.maakmsgprofiel .greenbtn');
	    }


	  });

	@if (Auth::user())

	$('body').on('click', 'a.makeactfav', function(e) {
	    	e.preventDefault();

	    	var $this = $(this);

	    	var actid = $(this).data('actid');

	    	$.ajax({
				url: '{{ url('makeActivFav') }}',
				type: 'get',
				data: {
					'user'   : {{ Auth::user()->qProfOID }},
					'activity' : actid
				},
				success: function(response) {
					$this.parent().html('<p class="activfav"><i class="glyphicon glyphicon-star"></i> FAVORIET</p>');
				}
			});
	    });

	@endif
	</script>
</body>
</html>
