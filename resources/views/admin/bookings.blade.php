@extends('app')

@section('content')

<div class="container">

	@if (session('flash_message'))
  	<div class="row">
    	<div class="col-xs-10 col-xs-offset-1">
      	  <div class="alert alert-success">{{ session('flash_message') }}</div>
      	</div>
  	</div>
 	@endif

  <div class="row">
    <div class="col-xs-10 col-xs-offset-1">
    	<a class="btn btn-primary pull-right" href="{{ url('admin/booking-add')}}">
        	<i class="fa fa-trash"></i> Add booking
      	</a>
    </div>
  </div>

  <hr>

  <div class="row">
    <form method="get">
     <div class="col-xs-3 col-xs-offset-1">
      <label for="activity">Activity</label>
      <input type="text" name="activity" class="form-control" value="{{ Request::get('activity') }}">
     </div>
     <div class="col-xs-3">
      <label for="user">User</label>
      <input type="text" name="user" class="form-control" value="{{ Request::get('user') }}">
     </div>
     <div class="col-xs-3 col-xs-offset-1">
      <label for="user">&nbsp;</label><br>
	  <input type="submit" class="btn btn-primary" value="Search">
     </div>
    </form>
  </div>

  <hr/>

  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">Booking list</div>

        <div class="panel-body">
          <table class="table table-striped">

            <thead>
              <th>Activity</th>
              <th>Booked by</th>
              <th>Booking date</th>
              <th>State</th>
              <th>&nbsp;</th>
              <th>&nbsp;</th>
            </thead>

            <tbody>
              @foreach ($bookings as $booking)
              <tr>
                <td>
                  <div> {{ App\AdminActivity::find($booking->qActivOID)->qTitle }}</div>
                </td>
                <td>
                  <div> {{ App\User::find($booking->qProfOIDBookingCreated)->qNameFirst . ' ' . App\User::find($booking->qProfOIDBookingCreated)->qNameLast }} </div>
                </td>
                <td>
                  <div>
                    {{ date('d.m.Y H:i', strtotime($booking->qCreatedAt)) }}
                  </div>
                </td>
              <td>
               <div>@if($booking->qStatus == 0) {{ 'Requested' }} @elseif ($booking->qStatus == 1) {{ 'Accepted' }} @else {{ 'Declined' }} @endif </div>
             </td>

             <td>
              <a href="{{ url('admin/booking-edit/'.$booking->qActivOID.'/'.$booking->qProfOIDBookingCreated) }}" class="btn btn-info">
               <i class="fa fa-pencil"></i> Update
             </a>
           </td>

           <td>
            <a href="{{ url('admin/booking-delete/'.$booking->qActivOID.'/'.$booking->qProfOIDBookingCreated) }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete booking?')">
             <i class="fa fa-trash"></i> Delete
           </a>

         </td>
       </tr>
       @endforeach
     </tbody>
   </table>

   {!! str_replace('/?', '?', $bookings->appends(['activity' => Request::get('activity'), 'user' => Request::get('user')])->render()) !!}

 </div>

</div>
</div>
</div>
</div>
@endsection
