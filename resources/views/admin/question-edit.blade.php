@extends('app')

@section('content')

<div class="container">

	<div class="row">
		<div class="col-md-6 col-md-offset-2 text-center">
			<h3>Edit question</h3>
		</div>
	</div>

	@if(count($errors))
	<div class="row">
		<div class="col-md-6 col-md-offset-2 text-center">
			<div class="alert alert-danger">
				@foreach($errors->all() as $error)
				<p>{{ ($error) }}</p>
				@endforeach
			</div>
		</div>
	</div>
	@endif

	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<form method="post" enctype="multipart/form-data" action="{{ url('admin/question-edit/'.$question->id) }}">

				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="title">Profile</label>
							<select name="profile" class="form-control">
								@foreach($users as $user)
								<option value="{{ $user->qProfOID }}" @if($user->qProfOID == $question->user_id){{ 'selected="selected"' }} @endif>{{ $user->qNameFirst . ' ' .$user->qNameLast }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="title">Title</label>
							<input type="text" class="form-control" name="title" value="{{ $question->questionTitle }}">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="text">Question</label>
							<textarea class="form-control" name="question" rows="5">{{ $question->questionText }}</textarea>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col-md-4">
						<input type="submit" class="btn btn-block btn-primary" value="Save">
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

@endsection
