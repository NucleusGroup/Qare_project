<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use App\User;
use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;

class AdminStatisticsController extends Controller {

    public function indexview()
    {
        $profiles = DB::table('profiles')->where('qIsAdmin', '=', 0)->get();
        $activities = DB::table('activities')->get();

        return view('admin.statistics', compact('profiles', 'activities'));
    }

    public function index()
    {
        $profiles = DB::table('profiles')->where('qIsAdmin', '=', 0)->get();
        $activities = DB::table('activities')->get();

        /*
         How many times has a user booked
        */
        if(Input::get('statistics')=='hmthub' && Input::get('user'))
        {
            $user = User::find(Input::get('user'));

            $profileview = DB::table('profileview')
                ->where('qProfOID', '=', $user->qProfOID)
                ->count();

            $ifuserchosesalwayssame = DB::table('activbookings')
                ->where('activbookings.qProfOIDBookingCreated', '=', $user->qProfOID)
                ->join('activities', 'activbookings.qActivOID', '=', 'activities.qActivOID')
                ->join('profiles', 'profiles.qProfOID', '=', 'activities.qProfOIDCreated')
                ->select('activbookings.*')
                ->groupBy('activities.qProfOIDCreated')
                ->get();

            $useractivities = DB::table('activities')
            ->join('profiles', 'profiles.qProfOID', '=', 'activities.qProfOIDCreated')
            ->where('profiles.qProfOID', '=', $user->qProfOID)
            ->whereRaw("DATE_FORMAT(activities.qCreatedAt, '%Y')='".date('Y')."'")
            ->count();

            $userbookings = DB::table('activbookings')
            ->join('activities', 'activbookings.qActivOID', '=', 'activities.qActivOID')
            ->join('profiles', 'profiles.qProfOID', '=', 'activities.qProfOIDCreated')
            ->where('profiles.qProfOID', '=', $user->qProfOID)
            ->count();

            $activitiesbymonth = DB::table('activities')
                ->join('profiles', 'profiles.qProfOID', '=', 'activities.qProfOIDCreated')
                ->where('profiles.qProfOID', '=', $user->qProfOID)
                ->select([
                    DB::raw("DATE_FORMAT(activities.qCreatedAt, '%Y-%m') AS `date`"),
                    DB::raw("COUNT(activities.qActivOID) AS `counter`")
                    ])
                ->whereRaw("DATE_FORMAT(activities.qCreatedAt, '%Y')='".date('Y')."'")
                ->groupBy('date')
                ->orderBy('date', 'ASC')
                ->get();

            $bookingsbymonth = DB::table('activbookings')
                ->join('activities', 'activbookings.qActivOID', '=', 'activities.qActivOID')
                ->join('profiles', 'profiles.qProfOID', '=', 'activities.qProfOIDCreated')
                ->where('profiles.qProfOID', '=', $user->qProfOID)
                ->select([
                    DB::raw("DATE_FORMAT(activbookings.qCreatedAt, '%Y-%m') AS `date`"),
                    DB::raw("COUNT(activbookings.qActivOID) AS `counter`")
                    ])
                ->groupBy('date')
                ->orderBy('date', 'ASC')
                ->get();

            $bookings = DB::table('activbookings')
                ->join('activities', 'activities.qActivOID', '=', 'activbookings.qActivOID')
                ->join('profiles', 'profiles.qProfOID', '=', 'activities.qProfOIDCreated')
                ->where('profiles.qProfOID', '=', Input::get('user'))
                ->count();



            return view('admin.statistics', compact('profiles', 'bookings', 'user', 'activities', 'userbookings', 'useractivities', 'activitiesbymonth', 'bookingsbymonth', 'ifuserchosesalwayssame', 'profileview'));
        }

        /*
            Which user has the most bookings
        */
        if(Input::get('statistics')=='wuhmb')
        {
            $mostbooking = DB::table('activbookings')
                ->select([
                    DB::raw("COUNT(activbookings.qActivOID) AS `counter`"),
                    DB::raw("activbookings.qActivOID AS `qActivOID`")
                    ])
                ->groupBy('activbookings.qActivOID')
                ->orderBy('counter', 'DESC')
                ->take(1)
                ->first();

            if(!$mostbooking)
            {
                Session::flash('error_message', 'No data at the moment.');
                return redirect()->back();
            }

            $activity = DB::table('activities')->where('qActivOID', '=', $mostbooking->qActivOID)->first();
            $user = DB::table('profiles')->where('qProfOID', '=', $activity->qProfOIDCreated)->first();

            $useractivities = DB::table('activities')
            ->join('profiles', 'profiles.qProfOID', '=', 'activities.qProfOIDCreated')
            ->where('profiles.qProfOID', '=', $user->qProfOID)
            ->whereRaw("DATE_FORMAT(activities.qCreatedAt, '%Y')='".date('Y')."'")
            ->count();

            $userbookings = DB::table('activbookings')
            ->join('activities', 'activbookings.qActivOID', '=', 'activities.qActivOID')
            ->join('profiles', 'profiles.qProfOID', '=', 'activities.qProfOIDCreated')
            ->where('profiles.qProfOID', '=', $user->qProfOID)
            ->count();

            $activitiesbymonth = DB::table('activities')
                ->join('profiles', 'profiles.qProfOID', '=', 'activities.qProfOIDCreated')
                ->where('profiles.qProfOID', '=', $user->qProfOID)
                ->select([
                    DB::raw("DATE_FORMAT(activities.qCreatedAt, '%Y-%m') AS `date`"),
                    DB::raw("COUNT(activities.qActivOID) AS `counter`")
                    ])
                ->whereRaw("DATE_FORMAT(activities.qCreatedAt, '%Y')='".date('Y')."'")
                ->groupBy('date')
                ->orderBy('date', 'ASC')
                ->get();

            $bookingsbymonth = DB::table('activbookings')
                ->join('activities', 'activbookings.qActivOID', '=', 'activities.qActivOID')
                ->join('profiles', 'profiles.qProfOID', '=', 'activities.qProfOIDCreated')
                ->where('profiles.qProfOID', '=', $user->qProfOID)
                ->select([
                    DB::raw("DATE_FORMAT(activbookings.qCreatedAt, '%Y-%m') AS `date`"),
                    DB::raw("COUNT(activbookings.qActivOID) AS `counter`")
                    ])
                ->groupBy('date')
                ->orderBy('date', 'ASC')
                ->get();

            $profileview = DB::table('profileview')
                ->where('qProfOID', '=', $user->qProfOID)
                ->count();

            return view('admin.statistics', compact('profiles', 'activity', 'user', 'mostbooking', 'activities', 'userbookings', 'useractivities', 'activitiesbymonth', 'bookingsbymonth', 'profileview'));
        }

        /*
        Which activities are booked the most
        */
        if(Input::get('statistics')=='wabm')
        {
            $mosttenbookings = DB::table('activbookings')
                ->select([
                    DB::raw("COUNT(activbookings.qActivOID) AS `counter`"),
                    DB::raw("activbookings.qActivOID AS `qActivOID`")
                    ])
                ->groupBy('activbookings.qActivOID')
                ->orderBy('counter', 'DESC')
                ->take(10)
                ->get();

                return view('admin.statistics', compact('profiles', 'mosttenbookings', 'activities'));
        }

        /*
        Number deleted accounts
        */
        if(Input::get('statistics')=='noda')
        {
            $deletedaccounts = DB::table('profiles')
                ->where('qIsDeleted', '=', 1)
                ->where('qIsAdmin', '=', 0)
                ->get();

                return view('admin.statistics', compact('profiles', 'deletedaccounts', 'activities'));
        }

        /*
        When are activities been booked
        */
        if(Input::get('statistics')=='waabb' && Input::get('activity'))
        {
            $singleactivity = DB::table('activities')->where('qActivOID', '=', Input::get('activity'))->first();
            $allbookings = DB::table('activbookings')->where('qActivOID', '=', $singleactivity->qActivOID)->get();

            return view('admin.statistics', compact('profiles', 'activities', 'singleactivity', 'allbookings'));
        }

        /*
        How many times are bookings being accepted or declined
        */
        if(Input::get('statistics')=='hmtabbaod')
        {
            $acceptedbookings = DB::table('activbookings')->where('qStatus', '=', 1)->count();
            $declinedbookings = DB::table('activbookings')->where('qStatus', '=', 2)->count();

            return view('admin.statistics', compact('profiles', 'activities', 'acceptedbookings', 'declinedbookings'));
        }


        /*
        Which ages are booking what activities
        */
        if(Input::get('statistics')=='waabwa' && Input::get('ages'))
        {
            if(Input::get('ages')=='under18')
            {
                $condition = "YEAR(profiles.qDateOfBirth)<='".date('Y')."' AND YEAR(profiles.qDateOfBirth)>'".(date('Y')-18)."'";
            }
            else if(Input::get('ages')=='18-30')
            {
                $condition = "YEAR(profiles.qDateOfBirth)<='".(date('Y')-18)."' AND YEAR(profiles.qDateOfBirth)>'".(date('Y')-30)."'";
            }
            else if(Input::get('ages')=='30-40')
            {
                $condition = "YEAR(profiles.qDateOfBirth)<='".(date('Y')-30)."' AND YEAR(profiles.qDateOfBirth)>'".(date('Y')-40)."'";
            }
            else if(Input::get('ages')=='40-50')
            {
                $condition = "YEAR(profiles.qDateOfBirth)<='".(date('Y')-40)."' AND YEAR(profiles.qDateOfBirth)>'".(date('Y')-50)."'";
            }
            else if(Input::get('ages')=='over50')
            {
                $condition = "YEAR(profiles.qDateOfBirth)<='".(date('Y')-50)."'";
            }

            $ages = DB::table('profiles')
                ->where('profiles.qIsAdmin', '=', 0)
                ->where('profiles.qIsDeleted', '=', 0)
                ->whereRaw($condition)
                ->join('activbookings', 'activbookings.qProfOIDBookingCreated', '=', 'profiles.qProfOID')
                ->join('activities', 'activities.qActivOID', '=', 'activbookings.qActivOID')
                ->selectRaw('activbookings.*, COUNT(activbookings.qActivOID) as `counter`')
                ->groupBy('activbookings.qActivOID')
                ->orderBy('counter', 'DESC')
                ->take(10)
                ->get();

            return view('admin.statistics', compact('profiles', 'activities', 'ages'));
        }


        /*
        Which age matches with what activity
        */
        if(Input::get('statistics')=='wamwwa' && Input::get('activity'))
        {
            $singleactivity = DB::table('activities')->where('qActivOID', '=', Input::get('activity'))->first();

            $bookings18 = DB::table('activbookings')
                ->join('profiles', 'profiles.qProfOID', '=', 'activbookings.qProfOIDBookingCreated')
                ->where('activbookings.qActivOID', '=', Input::get('activity'))
                ->whereRaw("YEAR(profiles.qDateOfBirth)<='".date('Y')."' AND YEAR(profiles.qDateOfBirth)>'".(date('Y')-18)."'")
                ->count();

            $bookings30 = DB::table('activbookings')
                ->join('profiles', 'profiles.qProfOID', '=', 'activbookings.qProfOIDBookingCreated')
                ->where('activbookings.qActivOID', '=', Input::get('activity'))
                ->whereRaw("YEAR(profiles.qDateOfBirth)<='".(date('Y')-18)."' AND YEAR(profiles.qDateOfBirth)>'".(date('Y')-30)."'")
                ->count();

            $bookings40 = DB::table('activbookings')
                ->join('profiles', 'profiles.qProfOID', '=', 'activbookings.qProfOIDBookingCreated')
                ->where('activbookings.qActivOID', '=', Input::get('activity'))
                ->whereRaw("YEAR(profiles.qDateOfBirth)<='".(date('Y')-30)."' AND YEAR(profiles.qDateOfBirth)>'".(date('Y')-40)."'")
                ->count();

            $bookings50 = DB::table('activbookings')
                ->join('profiles', 'profiles.qProfOID', '=', 'activbookings.qProfOIDBookingCreated')
                ->where('activbookings.qActivOID', '=', Input::get('activity'))
                ->whereRaw("YEAR(profiles.qDateOfBirth)<='".(date('Y')-40)."' AND YEAR(profiles.qDateOfBirth)>'".(date('Y')-50)."'")
                ->count();

            $bookingsover50 = DB::table('activbookings')
                ->join('profiles', 'profiles.qProfOID', '=', 'activbookings.qProfOIDBookingCreated')
                ->where('activbookings.qActivOID', '=', Input::get('activity'))
                ->whereRaw("YEAR(profiles.qDateOfBirth)<='".(date('Y')-50)."'")
                ->count();

            return view('admin.statistics', compact('profiles', 'activities', 'singleactivity', 'bookings18', 'bookings30', 'bookings40', 'bookings50', 'bookingsover50'));
        }

        /*
        Most popular tags
        */
        if(Input::get('statistics')=='mpt')
        {

            $populartags = DB::select(DB::raw("SELECT qTagOID, COUNT(qTagOID) AS `counter`
                    FROM
                    (
                        SELECT qTagOID FROM activtags
                        UNION ALL
                        SELECT qTagOID FROM proftags
                    ) AS alltags
                    GROUP BY qTagOID
                    ORDER BY counter DESC
                    LIMIT 10
            "));

            $unpopulartags = DB::select(DB::raw("SELECT qTagOID, COUNT(qTagOID) AS `counter`
                    FROM
                    (
                        SELECT qTagOID FROM activtags
                        UNION ALL
                        SELECT qTagOID FROM proftags
                    ) AS alltags
                    GROUP BY qTagOID
                    ORDER BY counter ASC
                    LIMIT 10
            "));

            return view('admin.statistics', compact('profiles', 'activities', 'populartags', 'unpopulartags'));
        }


        /*
        Total chats
        */
        else if(Input::get('statistics') == 'oc')
        {
            $chatrequests = DB::table('chat')->count();
            $chatrequestspending = DB::table('chat')->where('qStatus', '=', 0)->count();
            $chatrequestsaccepted = DB::table('chat')->where('qStatus', '=', 1)->count();
            $chatrequestsdeclined = DB::table('chat')->where('qStatus', '=', 2)->count();

            return view('admin.statistics', compact('profiles', 'activities', 'chatrequests', 'chatrequestsaccepted', 'chatrequestsdeclined', 'chatrequestspending'));
        }


        /*
        Total chats per user
        */
        else if(Input::get('statistics') == 'cpu' && Input::get('user'))
        {
            $currentuser = User::find(Input::get('user'));

            $profileview = DB::table('profileview')
                ->where('qProfOID', '=', $currentuser->qProfOID)
                ->count();

            //received
            $receivedchatrequests = DB::table('chat')->where('qProfOID', '=', Input::get('user'))->count();
            $receivedchatrequestspending = DB::table('chat')->where('qStatus', '=', 0)->where('qProfOID', '=', Input::get('user'))->count();
            $receivedchatrequestsaccepted = DB::table('chat')->where('qStatus', '=', 1)->where('qProfOID', '=', Input::get('user'))->count();
            $receivedchatrequestsdeclined = DB::table('chat')->where('qStatus', '=', 2)->where('qProfOID', '=', Input::get('user'))->count();

            //send
            $sendchatrequests = DB::table('chat')->where('qProfOIDRequested', '=', Input::get('user'))->count();
            $sendchatrequestspending = DB::table('chat')->where('qStatus', '=', 0)->where('qProfOIDRequested', '=', Input::get('user'))->count();
            $sendchatrequestsaccepted = DB::table('chat')->where('qStatus', '=', 1)->where('qProfOIDRequested', '=', Input::get('user'))->count();
            $sendchatrequestsdeclined = DB::table('chat')->where('qStatus', '=', 2)->where('qProfOIDRequested', '=', Input::get('user'))->count();

            return view('admin.statistics', compact('profiles', 'activities', 'receivedchatrequests', 'receivedchatrequestsaccepted', 'receivedchatrequestsdeclined', 'receivedchatrequestspending', 'sendchatrequests', 'sendchatrequestsaccepted', 'sendchatrequestsdeclined', 'sendchatrequestspending', 'currentuser', 'profileview'));
        }


        /*
        See reactions from users to users
        */
        else if(Input::get('statistics') == 'srfu' && Input::get('user'))
        {
            $currentuser = User::find(Input::get('user'));

            $profileview = DB::table('profileview')
                ->where('qProfOID', '=', $currentuser->qProfOID)
                ->count();

            $allchats = DB::table('chatmessages')
                ->where('qProfOIDSender', '=', Input::get('user'))
                ->orWhere('qProfOIDReceiver', '=', Input::get('user'))
                ->groupBy('qChatOID')
                ->get();

            return view('admin.statistics', compact('profiles', 'activities', 'currentuser', 'allchats', 'profileview'));
        }


        /*
        Most frequent searches in search
        */
        else if(Input::get('statistics') == 'mfsis')
        {
            $profsearch = DB::table('searchprof')
                ->orderBy('qCounter', 'DESC')
                ->take(5)
                ->get();

            $activsearch = DB::table('searchactiv')
                ->orderBy('qCounter', 'DESC')
                ->take(5)
                ->get();

            $favouritecounter = 0;
            $favourite = '';
            $dayNames = array(
                1 => 'Monday',
                2 => 'Tuesday',
                3 => 'Wednesday',
                4 => 'Thursday',
                5 => 'Friday',
                6 => 'Saturday',
                7 => 'Sunday'
             );
            for($i=1; $i<8; $i++)
            {
                $base = DB::table('searchactiv')->whereRaw("qDays LIKE '%[".$i."]%'")->count();

                if($base > $favouritecounter)
                {
                    $favouritecounter = $base;
                    $favourite = "Most favourite day for search is: ".$dayNames[$i]." with: ".$base." searches";
                }
            }


            return view('admin.statistics', compact('profiles', 'activities', 'profsearch', 'activsearch', 'favourite'));
        }

        /*
        Different matches between man/woman/woman/woman/man/
        */
        else if(Input::get('statistics') == 'dmbmw')
        {
            $manman = DB::table('activbookings')
                ->where('activbookings.qStatus', '=', 1)
                ->leftjoin('profiles as p1', 'p1.qProfOID', '=', 'activbookings.qProfOIDBookingCreated')
                ->where('p1.qGender', '=', 1)
                ->where('p1.qIsAdmin', '=', 0)
                ->leftjoin('activities', 'activities.qActivOID', '=', 'activbookings.qActivOID')
                ->join('profiles as p2', 'activities.qProfOIDCreated', '=', 'p2.qProfOID')
                ->where('p2.qGender', '=', 1)
                ->where('p2.qIsAdmin', '=', 0)
                ->count('activbookings.qActivOID');

            $womanwoman = DB::table('activbookings')
                ->where('activbookings.qStatus', '=', 1)
                ->leftjoin('profiles as p1', 'p1.qProfOID', '=', 'activbookings.qProfOIDBookingCreated')
                ->where('p1.qGender', '=', 2)
                ->where('p1.qIsAdmin', '=', 0)
                ->leftjoin('activities', 'activities.qActivOID', '=', 'activbookings.qActivOID')
                ->join('profiles as p2', 'activities.qProfOIDCreated', '=', 'p2.qProfOID')
                ->where('p2.qGender', '=', 2)
                ->where('p2.qIsAdmin', '=', 0)
                ->count('activbookings.qActivOID');

            $manwoman = DB::table('activbookings')
                ->where('activbookings.qStatus', '=', 1)
                ->leftjoin('profiles as p1', 'p1.qProfOID', '=', 'activbookings.qProfOIDBookingCreated')
                ->where('p1.qIsAdmin', '=', 0)
                ->leftjoin('activities', 'activities.qActivOID', '=', 'activbookings.qActivOID')
                ->join('profiles as p2', 'activities.qProfOIDCreated', '=', 'p2.qProfOID')
                ->where('p2.qIsAdmin', '=', 0)
                ->whereRaw("((p1.qGender=1 AND p2.qGender=2) OR (p1.qGender=2 AND p2.qGender=1))")
                ->count('activbookings.qActivOID');

            return view('admin.statistics', compact('profiles', 'activities', 'manman', 'womanwoman', 'manwoman'));


        }


        /*
        Admin didn't select statistics
        */
        else {
            return view('admin.statistics', compact('profiles', 'activities'));
        }




    }


    /*
    *Chats
    */
    public function userchat($id, $uid)
    {
        $messages = DB::table('chatmessages')
            ->where('qChatOID', '=', $id)
            ->first();

        return view('admin/monitor-chat', compact('messages', 'id', 'uid'));

    }


    /*
    *Real time monitoring chat
    */
    public function retrieveChatMessages($id, $uid, $suid, Request $request)
    {

        $chat = DB::table('chatmessages')
            ->where('qChatOID', '=', $id)
            ->get();

        $user = User::find($uid);

        $check = DB::table('chat')
        ->where(function($queryw) use ($user, $suid)
            {
                $queryw->where('qProfOIDRequested', '=', $user->qProfOID)
                       ->where('qProfOID', '=', $suid);
            })
        ->orWhere(function($query) use ($user, $suid)
            {
                $query->where('qProfOID', '=', $user->qProfOID)
                      ->where('qProfOIDRequested', '=', $suid);
            })
        ->where('qStatus', '=', 1)
        ->first();

        if(!$check)
        {
            return redirect('messages');
        }


        $message = DB::table('chatmessages')->where('qChatMessageOID', '>', $request->input('last_id'))->where('qProfOIDReceiver', '=', $suid)->where('qProfOIDSender', '=', $user->qProfOID)->orderBy('qCreatedAt', 'DESC')->first();





        if(count($message) > 0)
        {
            $returnhtml = '<input type="hidden" name="last_id" value="'.$message->qChatMessageOID.'">+++';

            DB::table('chatmessages')->where('qSeen', '=', 0)->where('qChatMessageOID', '=', $message->qChatMessageOID)->where('qProfOIDReceiver', '=', $suid)->update(['qSeen' => 1]);

            $sender = User::find($message->qProfOIDSender);

            if($sender->qPicture)
            {
                $image = '<img src="'.url('images/users/thumbs/'.$user->qPicture).'">';
            } else {
                if($sender->qGender==0 || $sender->qGender==1)
                {
                    $image = '<img src="'.url('img/user-male.png').'">';
                }
                else
                {
                    $image = '<img src="'.url('img/user-female.png').'">';
                }
            }

            if($sender->qProfOID == $uid)
            {
                $align = 'left';
            } else {
                $align = 'right';
            }

                if($align == 'left'){
                    $returnhtml .= '<div class="row message" style="text-align: '.$align.';">
                                        <div class="col-md-3">
                                          '.$image.'
                                          <p>'.$sender->qNameFirst.' '.$sender->qNameLast.'</p>
                                        </div>
                                        <div class="col-md-9">
                                          <p>'.date('d.m.Y. H:i:s', strtotime($message->qCreatedAt)).'</p>
                                          <p>'.$message->qMessage.'</p>
                                        </div>
                                    </div>';
                  } else {
                    $returnhtml .= '<div class="row message" style="text-align: '.$align.';">
                                      <div class="col-md-9">
                                          <p>'.date('d.m.Y. H:i:s', strtotime($message->qCreatedAt)).'</p>
                                          <p>'.$message->qMessage.'</p>
                                      </div>
                                      <div class="col-md-3">
                                          '.$image.'
                                          <p>'.$sender->qNameFirst.' '.$sender->qNameLast.'</p>
                                      </div>
                                  </div>';
                  }

            return $returnhtml;
        }

    }


}
